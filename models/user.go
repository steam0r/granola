package models

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	UUIDModel
	Email        string  `gorm:"default:null;not null;uniqueIndex:idx_uq_users_email"`
	Entries      []Entry `gorm:"foreignKey:UserID"`
	Handle       string  `gorm:"default:null;not null;"`
	PasswordHash string
}

func (user *User) BeforeCreate(tx *gorm.DB) error {
	if user.ID == uuid.Nil {
		user.ID = uuid.New()
	}
	return nil
}

func NewUser(db *gorm.DB, email string, handle string, password *string) (*User, error) {
	user := &User{Email: email, Handle: handle}
	if password != nil {
		if err := user.SetPassword(*password); err != nil {
			return nil, err
		}
	}

	if err := db.Create(user).Error; err != nil {
		return nil, err
	}

	return user, nil
}

func NewUserWithInvite(tx *gorm.DB, email string, handle string, password string, invite string) (*User, error) {
	if _, ok := tx.Statement.ConnPool.(*sql.Tx); !ok {
		return nil, fmt.Errorf("tx is not a transaction")
	}

	user, err := NewUser(tx, email, handle, &password);
	if err != nil {
		return nil, err
	}

	result := tx.Model(&Invite{}).Where("key = ? AND user_id is null", invite).Update("UserID", user.ID)
	if result.Error != nil || result.RowsAffected == 0 {
		return nil, errors.New("invalid invitation")
	}

	return user, nil
}

func (user *User) Path() string {
	return fmt.Sprintf("/users/%v", user.ID)
}

func (user *User) SetPassword(plainTextPassword string) error {
	password := []byte(plainTextPassword)

	hashedPassword, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	user.PasswordHash = string(hashedPassword)

	return nil
}

func (user *User) CheckPassword(plainTextPassword string) bool {
	if user.PasswordHash == "" {
		return false
	}

	password := []byte(plainTextPassword)
	hashedPassword := []byte(user.PasswordHash)
	err := bcrypt.CompareHashAndPassword(hashedPassword, password)
	return err == nil
}

func (user *User) MarshalJSON() ([]byte, error) {
	h := gin.H{
		"id":     user.Path(),
		"email":  user.Email,
		"handle": user.Handle,
	}

	if user.Entries != nil && len(user.Entries) > 0 {
		entries := make([]gin.H, len(user.Entries))
		for i, entry := range user.Entries {
			entries[i] = entry.AsMap()
		}
		h["entries"] = entries
	}

	return json.Marshal(h)
}

func (user *User) Votes(db *gorm.DB, entries []uuid.UUID) ([]*Vote, error) {
	if user == nil {
		return nil, nil
	}

	var votes []*Vote
	err := db.
		Preload("Entry").
		Preload("User").
		Where("user_id = ?", user.ID).
		Where("entry_id in (?)", entries).
		Group("entry_id").
		Having("created_at = max(created_at)").
		Find(&votes).
		Error

	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, err
	}

	return votes, err
}

func (user *User) CurrentVote(db *gorm.DB, entryID uuid.UUID) (*Vote, error) {
	var vote Vote
	result := db.
		Preload("Entry").
		Preload("User").
		Order("created_at desc").
		Where("user_id = ?", user.ID).
		Where("entry_id = ?", entryID).
		First(&vote)
	return &vote, result.Error
}
