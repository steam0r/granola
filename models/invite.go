package models

import (
	"encoding/json"
	"net/url"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/skip2/go-qrcode"
	"gorm.io/gorm"
)

type Invite struct {
	gorm.Model
	Key    string        `gorm:"not null;default:null;uniqueIndex:idx_uq_invites_key"`
	UserID uuid.NullUUID `gorm:"uniqueIndex:idx_uq_invites_userid"`
	User   User
}

func NewInvite(db *gorm.DB, key string) (*Invite, error) {
	invite := &Invite{
		Key: key,
	}
	result := db.Create(invite)
	return invite, result.Error
}

func (invite *Invite) MarshalJSON() ([]byte, error) {
	return json.Marshal(gin.H{
		"key": invite.Key,
	})
}

func (invite *Invite) RegisterURL(baseURL string) string {
	if b, err := url.Parse(baseURL); err != nil {
		panic(err)
	} else if u, err := url.Parse("/register?invite=" + invite.Key); err != nil {
		panic(err)
	} else {
		return b.ResolveReference(u).String()
	}
}

func (invite *Invite) QrCode(baseURL string) ([]byte, error) {
	return qrcode.Encode(invite.RegisterURL(baseURL), qrcode.Medium, 256)
}
