package models

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/yuin/goldmark"
	"gorm.io/gorm"
)

type Entry struct {
	UUIDModel
	Title    string `gorm:"default:null;not null"`
	Author   string `gorm:"default:null;not null"`
	Platform string
	Notes    string
	CompoID  uint `gorm:"default:null;not null"`
	UserID   uuid.UUID
	User     User
	Compo    Compo
	Locked   bool
	Uploads  []Upload
}

func (entry *Entry) BeforeCreate(tx *gorm.DB) error {
	if entry.ID == uuid.Nil {
		entry.ID = uuid.New()
	}
	return nil
}

func NewEntry(db *gorm.DB, title string, author string, platform string, notes string, compo *Compo, user *User, locked bool) (*Entry, error) {
	entry := &Entry{
		Title:    title,
		Author:   author,
		Platform: platform,
		Notes:    notes,
		Compo:    *compo,
		User:     *user,
		Locked:   locked,
	}

	if err := db.Create(entry).Error; err != nil {
		return nil, err
	}

	return entry, nil
}

func (entry *Entry) Path() string {
	return fmt.Sprintf("/entries/%v", entry.ID)
}

func (entry *Entry) PreviewPath() string {
	return fmt.Sprintf("%v/preview", entry.Path())
}

func (entry *Entry) NotesHtml() template.HTML {
	if entry.Notes == "" {
		return ""
	}

	md := []byte(entry.Notes)
	var b bytes.Buffer
	if err := goldmark.Convert(md, &b); err != nil {
		panic(err)
	}

	return template.HTML(b.Bytes())
}

func (entry *Entry) AsMap() gin.H {
	h := gin.H{
		"id":      entry.Path(),
		"title":   entry.Title,
		"author":  entry.Author,
		"locked":  entry.Locked,
		"preview": gin.H{"id": entry.PreviewPath()},
	}

	if entry.Notes != "" {
		h["notes"] = entry.Notes
	}

	if entry.Platform != "" {
		h["platform"] = entry.Platform
	}

	if entry.Compo.ID != 0 {
		h["compo"] = entry.Compo.Path()
	}

	if entry.User.ID != uuid.Nil {
		h["user"] = entry.User.Path()
	}

	return h
}

func (entry *Entry) MarshalJSON() ([]byte, error) {
	h := entry.AsMap()
	return json.Marshal(h)
}
