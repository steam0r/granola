package models

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type PlaylistEntry struct {
	PlaylistID uint      `gorm:"not null;default:null;primaryKey;autoIncrement:false"`
	EntryID    uuid.UUID `gorm:"not null;default:null;primaryKey;autoIncrement:false"`
	Entry      Entry
	Playlist   Playlist
	Order      uint         `gorm:"not null;default:0"`
	ShownAt    sql.NullTime `gorm:"nullable;default:null"`
}

type Playlist struct {
	gorm.Model
	Name               string       `gorm:"not null"`
	ShownAt            sql.NullTime `gorm:"nullable;default:null"`
	LiveVotingClosedAt sql.NullTime `gorm:"nullable;default:null"`
	VotingClosedAt     sql.NullTime `gorm:"nullable;default:null"`
	Entries            []PlaylistEntry
	Order      		   uint          `gorm:"not null;default:0"`
	ShowInPrizegiving  bool			 `gorm:"not null;default:true"`
}

type PlaylistPreview struct {
	Playlist
	SlideNumber int
}

type PlaylistShow struct {
	Playlist
	SlideNumber int
}

func NewPlaylist(db *gorm.DB, name string, entries []uuid.UUID) (*Playlist, error) {
	playlist := &Playlist{Name: name, Entries: []PlaylistEntry{}}

	err := db.Transaction(func(tx *gorm.DB) error {
		if err := tx.Create(playlist).Error; err != nil {
			return err
		}

		for idx, entryID := range entries {
			entry := &Entry{}
			// A foreign constraint should check this, but I can't seem to get that to work with SQLite.
			if err := tx.First(&entry, "id = ?", entryID).Error; err != nil {
				return err
			}

			e, err := NewPlaylistEntry(tx, playlist, entry, uint(idx))
			if err != nil {
				return err
			}
			playlist.Entries = append(playlist.Entries, *e)
		}
		return nil
	})

	if err != nil {
		return nil, err
	} else {
		return playlist, nil
	}
}

func (playlistEntry *PlaylistEntry) Equal(other *PlaylistEntry) bool {
	if playlistEntry == nil || other == nil {
		return false
	}

	if playlistEntry.PlaylistID != other.PlaylistID {
		return false
	}

	if playlistEntry.EntryID != other.EntryID {
		return false
	}

	if playlistEntry.Order != other.Order {
		return false
	}

	if playlistEntry.ShownAt != other.ShownAt {
		return false
	}

	if playlistEntry.Playlist.LiveVotingClosedAt != other.Playlist.LiveVotingClosedAt {
		return false
	}

	return true
}

func (playlist *Playlist) Path() string {
	return fmt.Sprintf("/playlists/%d", playlist.ID)
}

func (playlist *Playlist) VotingPath() string {
	return fmt.Sprintf("/voting/%d", playlist.ID)
}

func (playlist *Playlist) ArchivePath() string {
	return fmt.Sprintf("%v/archive", playlist.Path())
}

func (playlistPreview *PlaylistPreview) Path() string {
	return fmt.Sprintf("%v/preview", playlistPreview.Playlist.Path())
}

func (playlistShow *PlaylistShow) Path() string {
	return fmt.Sprintf("%v/show/%d", playlistShow.Playlist.Path(), playlistShow.SlideNumber)
}

func (playlist *Playlist) GetEntry(entryID uuid.UUID) *PlaylistEntry {
	for _, entry := range playlist.Entries {
		if entry.EntryID == entryID {
			return &entry
		}
	}

	return nil
}

func NewPlaylistEntry(db *gorm.DB, playlist *Playlist, entry *Entry, order uint) (*PlaylistEntry, error) {
	playlistEntry := &PlaylistEntry{
		Entry:      *entry,
		EntryID:    entry.ID,
		Playlist:   *playlist,
		PlaylistID: playlist.ID,
		Order:      order,
	}

	if err := db.Create(&playlistEntry).Error; err != nil {
		return nil, err
	}

	return playlistEntry, nil
}

func (playlistEntry *PlaylistEntry) Path() string {
	return fmt.Sprintf("%v/entries/%v", playlistEntry.Playlist.Path(), playlistEntry.EntryID)
}

func (playlistEntry *PlaylistEntry) VotesPath() string {
	return fmt.Sprintf("%v/votes", playlistEntry.Path())
}

func (playlistEntry *PlaylistEntry) VotingPath() string {
	return fmt.Sprintf("/voting/%d/%v", playlistEntry.Playlist.ID, playlistEntry.EntryID)
}

func (playlistEntry *PlaylistEntry) MarshalJSON() ([]byte, error) {
	data := playlistEntry.AsMap()
	return json.Marshal(data)
}

func (playlistEntry *PlaylistEntry) AsMap() gin.H {
	h := gin.H{
		"id":    playlistEntry.Path(),
		"order": playlistEntry.Order,
		"votes": gin.H{"id": playlistEntry.VotesPath()},
	}
	if playlistEntry.Entry.ID != uuid.Nil {
		h["entry"] = playlistEntry.Entry.AsMap()
	}
	if playlistEntry.Playlist.ID != 0 {
		h["playlist"] = playlistEntry.Playlist.AsMap()
	}
	if playlistEntry.ShownAt.Valid {
		h["shownAt"] = playlistEntry.ShownAt.Time
	}
	return h
}

func (playlist *Playlist) AsMap() gin.H {
	h := gin.H{
		"id":   playlist.Path(),
		"name": playlist.Name,
	}
	if playlist.ShownAt.Valid {
		h["shownAt"] = playlist.ShownAt.Time
	}
	if playlist.LiveVotingClosedAt.Valid {
		h["liveVotingClosedAt"] = playlist.LiveVotingClosedAt.Time
	}
	if playlist.VotingClosedAt.Valid {
		h["votingClosedAt"] = playlist.VotingClosedAt.Time
	}
	if len(playlist.Entries) > 0 {
		entries := make([]string, len(playlist.Entries))
		for i, entry := range playlist.Entries {
			entries[i] = entry.Path()
		}
		h["entries"] = entries
	}
	return h
}

func (playlist *Playlist) MarshalJSON() ([]byte, error) {
	h := playlist.AsMap()
	return json.Marshal(h)
}

func (playlistPreview *PlaylistPreview) MarshalJSON() ([]byte, error) {
	h := playlistPreview.AsMap()
	h["id"] = playlistPreview.Path()
	return json.Marshal(h)
}

func (playlistShow *PlaylistShow) MarshalJSON() ([]byte, error) {
	h := playlistShow.AsMap()
	h["id"] = playlistShow.Path()
	return json.Marshal(h)
}

func (playlist *Playlist) EntryIDs() []uuid.UUID {
	if playlist == nil || len(playlist.Entries) == 0 {
		return nil
	}

	entries := make([]uuid.UUID, len(playlist.Entries))
	for i, entry := range playlist.Entries {
		entries[i] = entry.EntryID
	}

	return entries
}

func (playlist *Playlist) IsVotingOpen() bool {
	return !playlist.VotingClosedAt.Valid
}

func (playlist *Playlist) IsVotingClosed() bool {
	return playlist.VotingClosedAt.Valid
}

func (playlist *Playlist) IsLiveVotingOpen() bool {
	return !playlist.LiveVotingClosedAt.Valid
}

func (playlist *Playlist) IsLiveVotingClosed() bool {
	return playlist.LiveVotingClosedAt.Valid
}

func (playlist *Playlist) VotingPlaylist(votes []*Vote) *VotingPlaylist {
	votingPlaylist := VotingPlaylist{}

	if playlist != nil && len(playlist.Entries) > 0 {
		votingPlaylist.Playlist = *playlist

		latestShownAt := func() time.Time {
			maxTime := playlist.Entries[0].ShownAt.Time
			for _, entry := range playlist.Entries {
				if entry.ShownAt.Time.After(maxTime) {
					maxTime = entry.ShownAt.Time
				}
			}
			return maxTime
		}()

		votingPlaylist.Entries = make([]VotingEntry, len(playlist.Entries))

		for i := range playlist.Entries {
			entryVote := func() *Vote {
				for _, vote := range votes {
					if vote.EntryID == playlist.Entries[i].EntryID {
						return vote
					}
				}

				return nil
			}()
			votingPlaylist.Entries[i] = VotingEntry{
				PlaylistEntry: playlist.Entries[i],
				IsCurrent:     playlist.Entries[i].ShownAt.Time.Equal(latestShownAt),
				Vote:          entryVote,
			}
		}
	}

	return &votingPlaylist
}

func (playlist *Playlist) Show(tx *gorm.DB) (*PlaylistShow, error) {
	if _, ok := tx.Statement.ConnPool.(*sql.Tx); !ok {
		return nil, fmt.Errorf("tx is not a transaction")
	}

	now := time.Now()
	playlist.ShownAt = sql.NullTime{Time: now, Valid: true}

	if err := tx.
		Model(&playlist).
		Omit("Entries").
		Where("id = ?", playlist.ID).
		Update("ShownAt", now).
		Error; err != nil {
		return nil, err
	}

	for i, playlistEntry := range playlist.Entries {
		if i == 0 {
			firstPlaylistEntry := playlist.Entries[0]

			if err := tx.
				Model(&firstPlaylistEntry).
				Where("entry_id = ? and playlist_id = ?",
					playlistEntry.EntryID,
					playlistEntry.PlaylistID).
				Update("ShownAt", now).
				Error; err != nil {
				return nil, err
			}
		}

		if err := tx.
			Model(&Entry{}).
			Where("id = ?", playlistEntry.EntryID).
			Update("Locked", true).
			Error; err != nil {
			return nil, err
		}

		if err := tx.
			Model(&Compo{}).
			Where("id = (?)", tx.
				Table("entries").
				Select("compo_id").
				Where("id = ?", playlistEntry.EntryID)).
			Update("Locked", true).
			Error; err != nil {
			return nil, err
		}
	}

	return &PlaylistShow{
		Playlist:    *playlist,
		SlideNumber: 1,
	}, nil
}

func (playlist *Playlist) Close(tx *gorm.DB) (*PlaylistShow, error) {
	if playlist == nil {
		return nil, fmt.Errorf("playlist is nil")
	}

	now := sql.NullTime{Time: time.Now(), Valid: true}

	// If all voting is still open, just close live voting.
	if playlist.IsVotingOpen() && playlist.IsLiveVotingOpen() {
		playlist.LiveVotingClosedAt = now
	} else if playlist.IsVotingOpen() {
		// Close offline voting.
		playlist.VotingClosedAt = now

		// If live voting for some reason is still open, close it as well.
		if !playlist.LiveVotingClosedAt.Valid {
			playlist.LiveVotingClosedAt = now
		}
	} else {
		// If all voting is already closed, we don't need to do anything
		return &PlaylistShow{Playlist: *playlist, SlideNumber: 1}, nil
	}

	if err := tx.Omit("Entries").Save(&playlist).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	return &PlaylistShow{Playlist: *playlist, SlideNumber: 1}, nil
}

func (playlist *Playlist) Open(tx *gorm.DB) (*PlaylistShow, error) {
	if playlist == nil {
		return nil, fmt.Errorf("playlist is nil")
	}

	nilTime := sql.NullTime{Time: time.Time{}, Valid: false}

	if playlist.IsVotingClosed() {
		// If offline voting is closed, open it.
		playlist.VotingClosedAt = nilTime
	} else if playlist.IsLiveVotingClosed() {
		// If live voting is closed, open it.
		playlist.LiveVotingClosedAt = nilTime
		// Also open offline voting.
		playlist.VotingClosedAt = nilTime
	} else {
		// If all voting is already open, we don't need to do anything
		return &PlaylistShow{Playlist: *playlist, SlideNumber: 1}, nil
	}

	if err := tx.Omit("Entries").Save(&playlist).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	return &PlaylistShow{Playlist: *playlist, SlideNumber: 1}, nil
}

func (playlistEntry *PlaylistEntry) Show(db *gorm.DB) error {
	playlistEntry.ShownAt = sql.NullTime{Time: time.Now(), Valid: true}

	if err := db.Save(&playlistEntry).Error; err != nil {
		return err
	}

	return nil
}
