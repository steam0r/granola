package models

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Upload struct {
	UUIDModel
	EntryID     string `gorm:"not null;default:null"`
	Entry       Entry
	Filename    string `gorm:"not null;default:null"`
	Content     []byte
	ContentSize int
	ContentType string `gorm:"not null;default:null"`
}

func (upload *Upload) BeforeCreate(tx *gorm.DB) error {
	if upload.ID == uuid.Nil {
		upload.ID = uuid.New()
	}
	return nil
}

func NewUpload(db *gorm.DB, filename string, content []byte, entry *Entry) (*Upload, error) {
	upload := &Upload{
		Filename:    filename,
		Content:     content,
		ContentType: http.DetectContentType(content),
		ContentSize: len(content),
		Entry:       *entry,
	}

	result := db.Create(&upload)
	return upload, result.Error
}

func (upload *Upload) IsImage() bool {
	return strings.HasPrefix(upload.ContentType, "image/")
}

func (upload *Upload) Path() string {
	return fmt.Sprintf("%v/uploads/%v", upload.Entry.Path(), upload.ID)
}

func (upload *Upload) MarshalJSON() ([]byte, error) {
	return json.Marshal(gin.H{
		"id":          upload.Path(),
		"entry":       upload.Entry.Path(),
		"filename":    upload.Filename,
		"contentType": upload.ContentType,
	})
}
