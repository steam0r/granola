package data

import (
	"log"
	"time"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	"granola/config"
	"granola/models"
)

func OpenDatabase(path string, logConfig config.LogConfig) *gorm.DB {
	gormConfig := gorm.Config{}
	ln := log.New(logConfig.Writer, "\r\n", log.LstdFlags)
	logLevel := logger.LogLevel(logConfig.Level)
	ignoreNotFound := logLevel == logger.Silent
	lc := logger.Config{
		SlowThreshold:             time.Second,
		LogLevel:                  logLevel,
		IgnoreRecordNotFoundError: ignoreNotFound,
		Colorful:                  true,
	}
	gormConfig.Logger = logger.New(ln, lc)

	if db, err := gorm.Open(sqlite.Open(path+"?cache=shared"), &gormConfig); err != nil {
		panic(err)
	} else if sql, err := db.DB(); err != nil {
		panic(err)
	} else if err = autoMigrate(db); err != nil {
		panic(err)
	} else {
		sql.SetMaxOpenConns(1)
		return db
	}
}

func autoMigrate(db *gorm.DB) error {
	modelTypes := []interface{}{
		models.User{},
		models.Invite{},
		models.Compo{},
		models.Entry{},
		models.Upload{},
		models.Playlist{},
		models.PlaylistEntry{},
		models.Vote{},
	}

	return db.Transaction(func(tx *gorm.DB) error {
		for _, model := range modelTypes {
			if err := tx.AutoMigrate(&model); err != nil {
				return err
			}
		}

		return nil
	})
}

func InitDatabase(db *gorm.DB, initialData *InitialData) {
	for _, compo := range initialData.Compos {
		if err := compo.Type.Scan(compo.Type); err != nil {
			panic(err)
		}
		if !compo.Type.IsValid() {
			panic("Invalid compo type")
		}

		_, err := models.NewCompo(db, compo.Name, compo.MultiPlatform, false, compo.Type, compo.Description)
		if err != nil {
			panic(err)
		}
	}

	for _, invite := range initialData.Invites {
		_, err := models.NewInvite(db, invite)
		if err != nil {
			panic(err)
		}
	}
}
