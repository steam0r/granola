package data

import "granola/models"

type InitialData struct {
	Compos  []InitialDataCompo
	Invites []string
}

type InitialDataCompo struct {
	Name          string           `yaml:"name"`
	Type          models.CompoType `yaml:"type"`
	MultiPlatform bool             `yaml:"multiplatform"`
	Description   string           `yaml:"desc"`
}
