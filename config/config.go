package config

type Config struct {
	Secret       []byte
	DatabasePath string
	Admins       map[string]string
	Logging      LogConfig
	Routing      struct {
		BaseURL               string
		MinimumPasswordLength int
	}
	RateLimit struct {
		Register uint32
		Login    uint32
	}
}
