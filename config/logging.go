package config

import (
	"io"
	"strings"
)

type LogLevel uint

const (
	Silent LogLevel = iota + 1
	Error
	Warn
	Info
)

type LogConfig struct {
	Writer io.Writer
	Level  LogLevel
}

func (logLevel *LogLevel) UnmarshalText(text []byte) error {
	logLevelMap := map[string]LogLevel{
		"silent": Silent,
		"error":  Error,
		"warn":   Warn,
		"info":   Info,
	}
	key := strings.ToLower(string(text))
	if l, ok := logLevelMap[key]; ok {
		*logLevel = l
	}

	return nil
}
