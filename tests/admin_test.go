package tests

import (
	"bytes"
	"fmt"
	"granola/models"
	"io"
	"mime/multipart"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestRoutes(t *testing.T) {
	r := NewTestRouterAdmin(t)

	response := r.adminRequest("GET", "/", nil)
	assert.Equal(t, http.StatusOK, response.StatusCode)
}

func TestAdminHome(t *testing.T) {
	r := NewTestRouterAdmin(t)
	r.createVotes()

	t.Run("GET / returns stats", func(t *testing.T) {
		response := r.adminRequest("GET", "/", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, float64(5), json["users"], "user count")
		assert.Equal(t, float64(25), json["entries"], "entry count")
		assert.Equal(t, float64(5), json["votes"], "vote count")
		assert.Equal(t, float64(17), json["score"], "score")
	})
}

func TestInvites(t *testing.T) {
	r := NewTestRouterAdmin(t)

	t.Run("GET /invites returns empty array", func(t *testing.T) {
		response := r.adminRequest("GET", "/invites", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("creating an invite works", func(t *testing.T) {
		response := r.adminRequest("POST", "/invites", gin.H{"key": "test-invite"})
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "test-invite", response.BodyJSON()["key"])
	})

	t.Run("GET /invites returns the created invite as JSON", func(t *testing.T) {
		response := r.adminRequest("GET", "/invites", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{"key":"test-invite"}]`, response.BodyString())
	})

	t.Run("GET /invites returns the created invite as HTML", func(t *testing.T) {
		response := r.adminRequestHTML("GET", "/invites")
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), "href=\"http://test-host/register?invite=test-invite\"")
	})

	t.Run("GET /invites with registered user does not crash", func(t *testing.T) {
		r.createUser("test-user@example.com", "test.user", "test-password", "test-invite")

		response := r.adminRequest("GET", "/invites", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{"key":"test-invite"}]`, response.BodyString())
	})
}

func TestUsers(t *testing.T) {
	r := NewTestRouterAdmin(t)

	t.Run("GET /users returns empty array", func(t *testing.T) {
		response := r.adminRequest("GET", "/users", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("POST /users with empty e-mail fails with 400", func(t *testing.T) {
		response := r.adminRequest("POST", "/users", gin.H{"email": "", "handle": "test-user"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "Email is a required field", json["detail"])
	})

	t.Run("POST /users with empty handle fails with 400", func(t *testing.T) {
		response := r.adminRequest("POST", "/users", gin.H{"email": "user@example.com", "handle": ""})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "Handle is a required field", json["detail"])
	})

	userID := "/users/52fdfc07-2182-454f-963f-5f0f9a621d72"
	t.Run("POST /users creates user", func(t *testing.T) {
		response := r.adminRequest("POST", "/users", gin.H{"email": "user@example.com", "handle": "test-user"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, userID, json["id"])
		assert.Equal(t, "user@example.com", json["email"])
		assert.Equal(t, "test-user", json["handle"])
	})

	t.Run("POST /users with duplicate e-mail fails with 400", func(t *testing.T) {
		response := r.adminRequest("POST", "/users", gin.H{"email": "user@example.com", "handle": "test-user2"})
		json := response.BodyJSON()
		// TODO: This should be 409 Conflict or something similar, not 500.
		assert.Equal(t, http.StatusInternalServerError, response.StatusCode)
		assert.Contains(t, json["detail"], "users.email")
	})

	t.Run("PUT /users/:id: updates user", func(t *testing.T) {
		response := r.adminRequest("PUT", userID, gin.H{"email": "user-updated@example.com", "handle": "test-user-updated"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, userID, json["id"])
		assert.Equal(t, "user-updated@example.com", json["email"])
		assert.Equal(t, "test-user-updated", json["handle"])
	})

	t.Run("DELETE /users/:id: deletes user", func(t *testing.T) {
		response := r.adminRequest("DELETE", userID, nil)
		assert.Equal(t, http.StatusNoContent, response.StatusCode)
	})

	t.Run("GET /users/:id: returns 404", func(t *testing.T) {
		response := r.adminRequest("GET", userID, nil)
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
	})
}

func TestUserEntries(t *testing.T) {
	r := NewTestRouterAdmin(t)
	user := r.createTestUser()
	compo := r.createTestCompo()
	entry := r.createUserTestEntry(compo, user)

	t.Run("GET /users/1 retrieves created user with entry", func(t *testing.T) {
		response := r.adminRequest("GET", user.Path(), nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, user.Path(), json["id"])
		assert.Equal(t, user.Email, json["email"])
		assert.Equal(t, user.Handle, json["handle"])
		assert.NotEmpty(t, json["entries"])
		e := json["entries"].([]any)[0].(map[string]any)
		assert.Equal(t, entry.Path(), e["id"])
		assert.Equal(t, entry.Title, e["title"])
		assert.Equal(t, entry.Author, e["author"])
		assert.Equal(t, entry.Platform, e["platform"])
		assert.Equal(t, entry.Notes, e["notes"])
	})
}

func TestComposEmpty(t *testing.T) {
	r := NewTestRouterAdmin(t)

	t.Run("GET /compos/ redirects to /compos", func(t *testing.T) {
		response := r.adminRequest("GET", "/compos/", nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, "/compos", response.Location)
	})

	t.Run("GET /compos returns empty array", func(t *testing.T) {
		response := r.adminRequest("GET", "/compos", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("GET /compos/1 returns 404", func(t *testing.T) {
		response := r.adminRequest("GET", "/compos/1", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("GET /compos/18446744073709551615 returns 404", func(t *testing.T) {
		response := r.adminRequest("GET", "/compos/18446744073709551615", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("GET /compos/340282366920938463463374607431768211455 returns 404", func(t *testing.T) {
		response := r.adminRequest("GET", "/compos/340282366920938463463374607431768211455", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})
}

func TestCreateCompo(t *testing.T) {
	r := NewTestRouterAdmin(t)

	t.Run("GET /compos returns empty array", func(t *testing.T) {
		response := r.adminRequest("GET", "/compos", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	compoPath := "/compos/1"
	t.Run("POST /compos creates a compo", func(t *testing.T) {
		compo := gin.H{
			"name":          "test compo",
			"multiplatform": true,
			"locked":        false,
			"type":          models.CompoTypeUndefined,
			"description":   ""}
		response := r.adminRequest("POST", "/compos", compo)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, compoPath, json["id"])
		assert.Equal(t, "test compo", json["name"])
		assert.Equal(t, true, json["multiPlatform"])
		assert.Equal(t, false, json["locked"])
		assert.Empty(t, json["type"])
	})

	t.Run("GET /compos returns created compo", func(t *testing.T) {
		response := r.adminRequest("GET", "/compos", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{
			"id":"`+compoPath+`",
			"name":"test compo",
			"multiPlatform":true,
			"locked":false
		}]`, response.BodyString())
	})

	t.Run("GET /compos/:id:/ redirects to /compos/:id:", func(t *testing.T) {
		response := r.adminRequest("GET", fmt.Sprintf("%v/", compoPath), nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, compoPath, response.Location)
	})

	t.Run("GET /compos/:id: returns created compo", func(t *testing.T) {
		response := r.adminRequest("GET", compoPath, nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		json := response.BodyJSON()
		assert.Equal(t, compoPath, json["id"])
		assert.Equal(t, "test compo", json["name"])
		assert.Equal(t, true, json["multiPlatform"])
		assert.Equal(t, false, json["locked"])
		assert.Empty(t, json["type"])
	})

	t.Run("DELETE /compos/1 works", func(t *testing.T) {
		response := r.adminRequest("DELETE", compoPath, nil)
		assert.Equal(t, http.StatusNoContent, response.StatusCode)
		assert.Empty(t, response.Body)
	})

	t.Run("GET /compos returns empty array", func(t *testing.T) {
		response := r.adminRequest("GET", "/compos", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("Create graphics compo works", func(t *testing.T) {
		compo := gin.H{
			"name":          "test compo 2",
			"multiplatform": false,
			"locked":        false,
			"type":          models.CompoTypeGraphics,
			"description":   "compo description"}
		response := r.adminRequest("POST", "/compos", compo)
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		json := response.BodyJSON()
		assert.Equal(t, "/compos/2", json["id"])
		assert.Equal(t, "test compo 2", json["name"])
		assert.Equal(t, false, json["multiPlatform"])
		assert.Equal(t, false, json["locked"])
		assert.Equal(t, "graphics", json["type"])
		assert.Equal(t, "compo description", json["description"])
	})
}

func TestUpdateCompo(t *testing.T) {
	r := NewTestRouterAdmin(t)
	compo := r.createTestCompo()

	t.Run("GET /compos returns created test compo", func(t *testing.T) {
		response := r.adminRequest("GET", "/compos", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{
			"id":"`+compo.Path()+`",
			"name":"test compo",
			"multiPlatform":true,
			"locked":false,
			"description":"test description"
		}]`, response.BodyString())
	})

	t.Run("updating compo works", func(t *testing.T) {
		response := r.adminRequest("PUT", compo.Path(), gin.H{
			"name":          "updated test compo",
			"multiPlatform": false,
			"locked":        true,
			"type":          models.CompoTypeMusic,
		})
		assert.Equal(t, http.StatusOK, response.StatusCode)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "updated test compo", json["name"])
		assert.Equal(t, false, json["multiPlatform"])
		assert.Equal(t, true, json["locked"])
		assert.Equal(t, "music", json["type"])
	})

	t.Run("GET /compos returns updated compo", func(t *testing.T) {
		response := r.adminRequest("GET", "/compos", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{
			"id":"`+compo.Path()+`",
			"name":"updated test compo",
			"multiPlatform":false,
			"locked":true,
			"type":"music"
		}]`, response.BodyString())
	})
}

func TestCompoArchive(t *testing.T) {
	r := NewTestRouterAdmin(t)
	compo := r.createTestCompo()
	entry := r.createTestEntry(compo)
	r.createUpload(entry, "test.txt", []byte("test-upload"))

	t.Run("GET compo archive returns expected ZIP file", func(t *testing.T) {
		response := r.adminRequest("GET", fmt.Sprintf("%v/archive", compo.Path()), nil)
		expectedZip := []byte("PK\x03\x04\x14\x00\b\x00\b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\b\x00\x00\x00test.txt*I-.\xd1--\xc8\xc9OL\x01\x04\x00\x00\xff\xffPK\a\b3 \x93\xfb\x11\x00\x00\x00\v\x00\x00\x00PK\x01\x02\x14\x00\x14\x00\b\x00\b\x00\x00\x00\x00\x003 \x93\xfb\x11\x00\x00\x00\v\x00\x00\x00\b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00test.txtPK\x05\x06\x00\x00\x00\x00\x01\x00\x01\x006\x00\x00\x00G\x00\x00\x00\x00\x00")
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, expectedZip, response.Body)
	})
}

func TestCreateEntry(t *testing.T) {
	r := NewTestRouterAdmin(t)

	compo := r.createTestCompo()

	t.Run("GET /entries returns empty array", func(t *testing.T) {
		response := r.adminRequest("GET", "/entries", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	expectedID := "/entries/52fdfc07-2182-454f-963f-5f0f9a621d72"
	t.Run("POST /entries creates an entry", func(t *testing.T) {
		response := r.adminRequest("POST", fmt.Sprintf("%v/entries", compo.Path()), gin.H{
			"title":    "test-entry",
			"author":   "test-author",
			"platform": "test-platform",
			"notes":    "test-notes",
		})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, expectedID, json["id"])
		assert.Equal(t, compo.Path(), json["compo"])
		assert.Equal(t, "test-author", json["author"])
		assert.Equal(t, "test-entry", json["title"])
		assert.Equal(t, "test-platform", json["platform"])
		assert.Equal(t, "test-notes", json["notes"])
		assert.Equal(t, false, json["locked"])
		assert.Equal(t, fmt.Sprintf("%v/preview", expectedID), json["preview"].(map[string]any)["id"])
	})

	t.Run("GET /entries returns array with created entry", func(t *testing.T) {
		response := r.adminRequest("GET", "/entries", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{
			"id":"`+expectedID+`",
			"compo":"`+compo.Path()+`",
			"author":"test-author",
			"title":"test-entry",
			"platform":"test-platform",
			"notes":"test-notes",
			"locked":false,
			"preview": { "id": "`+expectedID+`/preview" }
		}]`, response.BodyString())
	})

	t.Run("GET /entries/1 returns created entry", func(t *testing.T) {
		response := r.adminRequest("GET", expectedID, nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, expectedID, json["id"])
		assert.Equal(t, compo.Path(), json["compo"])
		assert.Equal(t, "test-author", json["author"])
		assert.Equal(t, "test-entry", json["title"])
		assert.Equal(t, false, json["locked"])
		assert.Equal(t, fmt.Sprintf("%v/preview", expectedID), json["preview"].(map[string]any)["id"])
	})

	t.Run("GET /entries/2 returns 404", func(t *testing.T) {
		response := r.adminRequest("GET", "/entries/2", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("creating entry for non-existent compo fails with 404", func(t *testing.T) {
		response := r.adminRequest("POST", "/compos/2/entries", gin.H{
			"title":    "test-entry",
			"author":   "test-author",
			"platform": "",
			"notes":    "",
		})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})
}

func TestUpdateEntry(t *testing.T) {
	r := NewTestRouterAdmin(t)

	compo := r.createTestCompo()
	entry := r.createTestEntry(compo)

	t.Run("updating non-existing entry fails with 404", func(t *testing.T) {
		response := r.adminRequest("PUT", "/entries/2", gin.H{
			"compo":  1,
			"author": "test-author",
			"title":  "updated test-entry",
			"user":   0,
			"locked": false,
		})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("updating existing entry works", func(t *testing.T) {
		response := r.adminRequest("PUT", entry.Path(), gin.H{
			"compo":  1,
			"author": "updated test-author",
			"title":  "updated test-entry",
			"user":   0,
			"locked": true,
		})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, entry.Path(), json["id"])
		assert.Equal(t, compo.Path(), json["compo"])
		assert.Equal(t, "updated test-author", json["author"])
		assert.Equal(t, "updated test-entry", json["title"])
		assert.Equal(t, true, json["locked"])
		assert.Equal(t, entry.PreviewPath(), json["preview"].(map[string]any)["id"])
	})
}

func TestDeleteEntry(t *testing.T) {
	r := NewTestRouterAdmin(t)

	compo := r.createTestCompo()
	entry := r.createTestEntry(compo)

	t.Run("deleting non-existing entry fails with 404", func(t *testing.T) {
		response := r.adminRequest("DELETE", "/entries/2", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("deleting existing entry works", func(t *testing.T) {
		response := r.adminRequest("DELETE", entry.Path(), nil)
		assert.Equal(t, http.StatusNoContent, response.StatusCode)
	})
}

func TestAdminEntryPreview(t *testing.T) {
	r := NewTestRouterAdmin(t)
	compo := r.createTestCompo()
	entry := r.createTestEntry(compo)

	t.Run("GET returns expected HTML", func(t *testing.T) {
		response := r.adminRequest("GET", entry.PreviewPath(), nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), "test-entry</h1>")
		assert.Contains(t, response.BodyString(), "<h2>By test-author</h2>")
	})
}

func TestCreateUpload(t *testing.T) {
	r := NewTestRouterAdmin(t)

	compo := r.createTestCompo()
	entry := r.createTestEntry(compo)

	createUpload := func(entryPath string, filename string, data []byte) TestResponse {
		body := new(bytes.Buffer)
		writer := multipart.NewWriter(body)
		part, _ := writer.CreateFormFile("file", filename)
		if _, err := part.Write(data); err != nil {
			panic(err)
		}
		if err := writer.Close(); err != nil {
			panic(err)
		}
		path := fmt.Sprintf("%v/uploads", entryPath)

		req, _ := http.NewRequest("POST", path, body)
		req.SetBasicAuth("admin", "password")
		req.Header.Set("Content-Type", writer.FormDataContentType())
		w := CreateTestResponseRecorder()
		r.Engine.ServeHTTP(w, req)

		responseData, _ := io.ReadAll(w.Body)
		return TestResponse{
			StatusCode:  w.Code,
			Body:        responseData,
			ContentType: w.Header().Get("Content-Type"),
			Location:    w.Header().Get("Location"),
		}
	}

	uploadsPath := fmt.Sprintf("%v/uploads", entry.Path())

	t.Run("GET uploads returns empty array", func(t *testing.T) {
		response := r.adminRequest("GET", uploadsPath, nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("creating upload for non-existing entry fails with 404", func(t *testing.T) {
		response := createUpload("/entries/2", "test.txt", []byte("test-upload"))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	expectedUpload1 := "9566c74d-1003-4c4d-bbbb-0407d1e2c649"
	t.Run("creating upload for existing entry works", func(t *testing.T) {
		response := createUpload(entry.Path(), "test.txt", []byte("test-upload"))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, fmt.Sprintf("%v/%v", uploadsPath, expectedUpload1), json["id"])
		assert.Equal(t, entry.Path(), json["entry"])
		assert.Equal(t, "test.txt", json["filename"])
		assert.Equal(t, "text/plain; charset=utf-8", json["contentType"])
	})

	expectedUpload2 := "6694d2c4-22ac-4208-a007-2939487f6999"
	t.Run("creating upload for another entry works", func(t *testing.T) {
		entry2 := r.createEntry(compo, "test-entry-2", "test-author-2", "", "")
		response := createUpload(entry2.Path(), "test.txt", []byte("test-upload"))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, fmt.Sprintf("%v/uploads/%v", entry2.Path(), expectedUpload2), json["id"])
		assert.Equal(t, entry2.Path(), json["entry"])
	})

	t.Run("GET uploads returns array with created upload for entry 1", func(t *testing.T) {
		path := fmt.Sprintf("%v/uploads", entry.Path())
		response := r.adminRequest("GET", path, nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{
			"contentType":"text/plain; charset=utf-8",
			"entry":"`+entry.Path()+`",
			"filename":"test.txt",
			"id":"`+fmt.Sprintf("%v/%v", path, expectedUpload1)+`"
		}]`, response.BodyString())
	})

	t.Run("GET upload returns created upload", func(t *testing.T) {
		id := fmt.Sprintf("%v/%v", uploadsPath, expectedUpload1)
		response := r.adminRequest("GET", id, nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, id, json["id"])
		assert.Equal(t, entry.Path(), json["entry"])
		assert.Equal(t, "test.txt", json["filename"])
		assert.Equal(t, "text/plain; charset=utf-8", json["contentType"])
	})

	t.Run("GET upload-data returns uploaded data", func(t *testing.T) {
		response := r.adminRequest("GET", fmt.Sprintf("%v/%v/data", uploadsPath, expectedUpload1), nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "test-upload", response.BodyString())
	})

	t.Run("GET upload returns 404", func(t *testing.T) {
		response := r.adminRequest("GET", "/entries/0/uploads/1", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("GET upload-data returns 404", func(t *testing.T) {
		response := r.adminRequest("GET", "/entries/0/uploads/1/data", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})
}

func TestDeleteUpload(t *testing.T) {
	r := NewTestRouterAdmin(t)

	compo := r.createTestCompo()
	entry := r.createTestEntry(compo)
	upload := r.createUpload(entry, "test.txt", []byte("test-upload"))

	t.Run("DELETE upload fails with 404", func(t *testing.T) {
		response := r.adminRequest("DELETE", "/entries/0/uploads/1", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("DELETE created upload succeeds", func(t *testing.T) {
		response := r.adminRequest("DELETE", upload.Path(), nil)
		assert.Equal(t, http.StatusNoContent, response.StatusCode)
		assert.Empty(t, response.Body)
	})

	t.Run("GET uploads returns empty array", func(t *testing.T) {
		response := r.adminRequest("GET", fmt.Sprintf("%v/uploads", entry.Path()), nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("DELETE already deleted uploads fails with 404", func(t *testing.T) {
		response := r.adminRequest("DELETE", upload.Path(), nil)
		json := response.BodyJSON()
		// TODO: This should preferably return `410 Gone`, since the resource once did exist.
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})
}

func TestUserAdministration(t *testing.T) {
	r := NewTestRouterAdmin(t)

	t.Run("GET /users/ redirects to /users", func(t *testing.T) {
		response := r.adminRequest("GET", "/users/", nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, "/users", response.Location)
	})

	t.Run("GET /users returns empty array", func(t *testing.T) {
		response := r.adminRequest("GET", "/users", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	userPath := "/users/52fdfc07-2182-454f-963f-5f0f9a621d72"
	t.Run("POST /users creates user", func(t *testing.T) {
		response := r.adminRequest("POST", "/users", gin.H{"email": "user1@example.com", "handle": "test-user"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, userPath, json["id"])
		assert.Equal(t, "user1@example.com", json["email"])
		assert.Equal(t, "test-user", json["handle"])
	})

	t.Run("GET /users returns created user", func(t *testing.T) {
		response := r.adminRequest("GET", "/users", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{
			"id":"`+userPath+`",
			"email":"user1@example.com",
			"handle":"test-user"
		}]`, response.BodyString())
	})

	t.Run("GET /users/:id:/ redirects to /users/:id:", func(t *testing.T) {
		response := r.adminRequest("GET", fmt.Sprintf("%v/", userPath), nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, userPath, response.Location)
	})

	t.Run("GET /users/:id: returns created user", func(t *testing.T) {
		response := r.adminRequest("GET", userPath, nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, userPath, json["id"])
		assert.Equal(t, "user1@example.com", json["email"])
		assert.Equal(t, "test-user", json["handle"])
	})

	t.Run("PUT /users/:id: updates user", func(t *testing.T) {
		response := r.adminRequest("PUT", userPath, gin.H{"email": "user2@example.com", "handle": "test-user2"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, userPath, json["id"])
		assert.Equal(t, "user2@example.com", json["email"])
		assert.Equal(t, "test-user2", json["handle"])
	})

	t.Run("DELETE /users/:id: succeeds", func(t *testing.T) {
		response := r.adminRequest("DELETE", userPath, gin.H{"email": "user2@example.com", "handle": "test-user2"})
		assert.Equal(t, http.StatusNoContent, response.StatusCode)
	})
}

func TestGetPlaylists(t *testing.T) {
	r := NewTestRouterAdmin(t)

	t.Run("GET /playlists returns empty array", func(t *testing.T) {
		response := r.adminRequest("GET", "/playlists", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("GET non-existing playlist returns 404", func(t *testing.T) {
		response := r.adminRequest("GET", "/playlists/1", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	playlist := r.createPlaylist("Playlist 1", nil)

	t.Run("GET /playlists returns array with created playlist", func(t *testing.T) {
		response := r.adminRequest("GET", playlist.Path(), nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, playlist.Path(), json["id"])
		assert.Equal(t, "Playlist 1", json["name"])
	})
}

func TestPostPlaylists(t *testing.T) {
	r := NewTestRouterAdmin(t)

	playlistPath := "/playlists/1"
	t.Run("POST /playlists creates playlist", func(t *testing.T) {
		response := r.adminRequest("POST", "/playlists", gin.H{"name": "Playlist 1"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, playlistPath, json["id"])
		assert.Equal(t, "Playlist 1", json["name"])
	})

	t.Run("POST /playlists with invalid UUID", func(t *testing.T) {
		response := r.adminRequest("POST", "/playlists", gin.H{"name": "Playlist 1", "entries": []gin.H{{"entry": "not-an-uuid"}}})
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "invalid UUID length: 11", response.BodyJSON()["detail"])
	})

	playlistPath2 := "/playlists/2"
	t.Run("POST /playlists creates playlist", func(t *testing.T) {
		response := r.adminRequest("POST", "/playlists", gin.H{"name": "Playlist 2"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, playlistPath2, json["id"])
		assert.Equal(t, "Playlist 2", json["name"])
	})

	compo := r.createTestCompo()
	entry := r.createTestEntry(compo)

	playlistPath3 := "/playlists/3"
	t.Run("POST /playlists creates playlist", func(t *testing.T) {
		response := r.adminRequest("POST", "/playlists", gin.H{"name": "Playlist 3", "entries": []gin.H{{"entry": entry.ID}}})
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.JSONEq(t, `{
			"id":"`+playlistPath3+`",
			"name":"Playlist 3",
			"entries": ["`+fmt.Sprintf("%v/entries/%v", playlistPath3, entry.ID)+`"]
		}`, response.BodyString())
	})
}

func TestPostPlaylistsEntry(t *testing.T) {
	r := NewTestRouterAdmin(t)

	playlist1 := r.createPlaylist("Playlist 1", nil)

	t.Run("POST playlist entry with reference to non-existing entry fails with 404", func(t *testing.T) {
		// This is a randomly generated UUID, and we should never collide with it.
		invalidUUID := "3e7591d1-9e42-4054-b004-8e5bd9b8a1f0"

		response := r.adminRequest("POST", playlist1.Path()+"/entries", gin.H{"entry": invalidUUID})
		json := response.BodyJSON()
		// TODO: It's not actually correct to fail with 404 for references that don't exist.
		//       404 only applies to the resource identified by the URI. As long as the URI
		//       resolves, 404 is not the correct response. We should respond with 400 or 422
		//       here (and everywhere else with similar error handling) instead.
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("POST playlist entry with reference to invalid UUID fails with 404", func(t *testing.T) {
		response := r.adminRequest("POST", playlist1.Path()+"/entries", gin.H{"entry": "not-an-uuid"})
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "invalid UUID length: 11", response.BodyJSON()["detail"])
	})

	compo := r.createTestCompo()
	entry := r.createTestEntry(compo)

	t.Run("POST playlist entry with reference to existing entry creates playlist entry", func(t *testing.T) {
		response := r.adminRequest("POST", playlist1.Path()+"/entries", gin.H{"entry": entry.ID})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, fmt.Sprintf("%v/entries/%v", playlist1.Path(), entry.ID), json["id"])
		assert.Equal(t, float64(0), json["order"])
		assert.Equal(t, playlist1.Path(), json["playlist"].(map[string]any)["id"])
		assert.Equal(t, fmt.Sprintf("%v/entries/%v/votes", playlist1.Path(), entry.ID), json["votes"].(map[string]any)["id"])
		assert.Equal(t, map[string]any{
			"author":   "test-author",
			"id":       entry.Path(),
			"locked":   false,
			"title":    "test-entry",
			"notes":    "test-notes",
			"platform": "test-platform",
			"preview":  map[string]any{"id": entry.PreviewPath()},
		}, json["entry"])
	})

	playlist3 := r.createPlaylist("Playlist 3", []uuid.UUID{entry.ID})
	entry2 := r.createTestEntry(compo)

	t.Run("POST playlist entry succeeds", func(t *testing.T) {
		response := r.adminRequest("POST", playlist3.Path()+"/entries", gin.H{"entry": entry2.ID})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, gin.H{
			"entry": map[string]any{
				"author":   "test-author",
				"id":       entry2.Path(),
				"locked":   false,
				"title":    "test-entry",
				"notes":    "test-notes",
				"platform": "test-platform",
				"preview":  map[string]any{"id": entry2.PreviewPath()},
			},
			"id":       fmt.Sprintf("%v/entries/%v", playlist3.Path(), entry2.ID),
			"order":    float64(1),
			"playlist": map[string]any{"id": playlist3.Path(), "name": "Playlist 3"},
			"votes":    map[string]any{"id": fmt.Sprintf("%v/entries/%v/votes", playlist3.Path(), entry2.ID)},
		}, json)
	})

	entry3 := r.createEntry(compo, "test-entry2", "test-author2", "", "")

	t.Run("POST playlist entry succeeds", func(t *testing.T) {
		response := r.adminRequest("POST", playlist3.Path()+"/entries", gin.H{"entry": entry3.ID})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, gin.H{
			"entry": map[string]any{
				"author":  "test-author2",
				"id":      entry3.Path(),
				"locked":  false,
				"title":   "test-entry2",
				"preview": map[string]any{"id": entry3.PreviewPath()},
			},
			"id":    fmt.Sprintf("%v/entries/%v", playlist3.Path(), entry3.ID),
			"order": float64(2),
			"playlist": map[string]any{
				"id":   playlist3.Path(),
				"name": "Playlist 3",
			},
			"votes": map[string]any{
				"id": fmt.Sprintf("%v/entries/%v/votes", playlist3.Path(), entry3.ID),
			},
		}, json)
	})

	entry4 := r.createTestEntry(compo)
	t.Run("POST HTML playlist entry succeeds", func(t *testing.T) {
		path := fmt.Sprintf("%v/entries", playlist3.Path())
		data := map[string]string{"entry": entry4.ID.String()}
		response := r.adminPostFormData(path, data)
		assert.Equal(t, http.StatusCreated, response.StatusCode)
	})
}

func TestPlaylists(t *testing.T) {
	r := NewTestRouterAdmin(t)

	compo := r.createTestCompo()
	entry := r.createTestEntry(compo)
	playlist1 := r.createPlaylist("Playlist 1", []uuid.UUID{ entry.ID })

	t.Run("DELETE created playlist entry succeeds", func(t *testing.T) {
		response := r.adminRequest("DELETE", fmt.Sprintf("%v/entries/%v", playlist1.Path(), entry.ID), nil)
		assert.Equal(t, http.StatusNoContent, response.StatusCode)
	})

	t.Run("DELETE created playlist succeeds", func(t *testing.T) {
		response := r.adminRequest("DELETE", playlist1.Path(), nil)
		assert.Equal(t, http.StatusNoContent, response.StatusCode)
	})

	playlist2 := r.createPlaylist("Playlist 2", nil)
	t.Run(`PATCH /playlists/2 with "action=show" shows playlist`, func(t *testing.T) {
		response := r.adminRequest("PATCH", playlist2.Path(), gin.H{"action": "show"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["shownAt"])
	})

	entry2 := r.createTestEntry(compo)
	entry3 := r.createEntry(compo, "test-entry2", "test-author2", "", "")
	playlist3 := r.createPlaylist("Playlist 3", []uuid.UUID{entry.ID, entry2.ID, entry3.ID})

	t.Run("GET /playlists/3/entries returns array with created playlist entries", func(t *testing.T) {
		response := r.adminRequest("GET", playlist3.Path()+"/entries", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{
			"entry":{
				"author":"test-author",
				"id":"`+entry.Path()+`",
				"locked":false,
				"title":"test-entry",
				"notes":"test-notes",
				"platform":"test-platform",
				"preview": { "id": "`+entry.Path()+`/preview" }
			},
			"id":"`+fmt.Sprintf("%v/entries/%v", playlist3.Path(), entry.ID)+`",
			"order":0,
			"playlist":{"id":"`+playlist3.Path()+`", "name":"Playlist 3"},
			"votes": {"id":"`+fmt.Sprintf("%v/entries/%v/votes", playlist3.Path(), entry.ID)+`"}
		}, {
			"entry":{
				"author":"test-author",
				"id":"`+entry2.Path()+`",
				"locked":false,
				"title":"test-entry",
				"notes":"test-notes",
				"platform":"test-platform",
				"preview": { "id": "`+entry2.Path()+`/preview" }
			},
			"id":"`+fmt.Sprintf("%v/entries/%v", playlist3.Path(), entry2.ID)+`",
			"order":1,
			"playlist":{"id":"`+playlist3.Path()+`", "name":"Playlist 3"},
			"votes": {"id":"`+fmt.Sprintf("%v/entries/%v/votes", playlist3.Path(), entry2.ID)+`"}
		}, {
			"entry":{
				"author":"test-author2",
				"id":"`+entry3.Path()+`",
				"locked":false,
				"title":"test-entry2",
				"preview": { "id": "`+entry3.Path()+`/preview" }
			},
			"id":"`+fmt.Sprintf("%v/entries/%v", playlist3.Path(), entry3.ID)+`",
			"order":2,
			"playlist":{"id":"`+playlist3.Path()+`", "name":"Playlist 3"},
			"votes": {"id":"`+fmt.Sprintf("%v/entries/%v/votes", playlist3.Path(), entry3.ID)+`"}
		}]`, response.BodyString())
	})

	t.Run(`PATCH playlist entry with "direction=down" increments "order"`, func(t *testing.T) {
		response := r.adminRequest("PATCH", fmt.Sprintf("%v/entries/%v", playlist3.Path(), entry.ID), gin.H{"direction": "down"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, float64(1), json["order"])
	})

	t.Run("GET playlist preview succeeds", func(t *testing.T) {
		response := r.adminRequest("GET", playlist3.Path()+"/preview", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, playlist3.Path()+"/preview", json["id"])
		assert.Equal(t, "Playlist 3", json["name"])
		entries := json["entries"].([]any)
		assert.Equal(t, 3, len(entries))
		assert.Equal(t, fmt.Sprintf("%v/entries/%v", playlist3.Path(), entry2.ID), entries[0])
		assert.Equal(t, fmt.Sprintf("%v/entries/%v", playlist3.Path(), entry.ID), entries[1])
	})

	t.Run("GET playlist preview with out of bounds slide numbers succeeds", func(t *testing.T) {
		for i := 0; i < 4; i++ {
			response := r.adminRequest("GET", fmt.Sprintf("%v/preview/%d", playlist3.Path(), i), nil)
			json := response.BodyJSON()
			assert.Equal(t, http.StatusOK, response.StatusCode)
			assert.Equal(t, playlist3.Path()+"/preview", json["id"])
			assert.Equal(t, "Playlist 3", json["name"])
			entries := json["entries"].([]any)
			assert.Equal(t, 3, len(entries))
			assert.Equal(t, fmt.Sprintf("%v/entries/%v", playlist3.Path(), entry2.ID), entries[0])
			assert.Equal(t, fmt.Sprintf("%v/entries/%v", playlist3.Path(), entry.ID), entries[1])
			assert.Equal(t, fmt.Sprintf("%v/entries/%v", playlist3.Path(), entry3.ID), entries[2])
		}
	})

	t.Run(`PATCH playlist entry with "action=invalid" fails with 400`, func(t *testing.T) {
		response := r.adminRequest("PATCH", playlist3.Path(), gin.H{"action": "invalid"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "invalid action: invalid", json["detail"])
	})

	t.Run(`PATCH playlist with "action=show" shows playlist`, func(t *testing.T) {
		response := r.adminRequest("PATCH", playlist3.Path(), gin.H{"action": "show"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["shownAt"])
	})

	t.Run("compo should be locked after having one or more of its entries shown in a playlist", func(t *testing.T) {
		response := r.adminRequest("GET", compo.Path(), nil)
		json := response.BodyJSON()
		assert.Equal(t, true, json["locked"])
	})

	t.Run("entry 1 should be locked after being shown in a playlist", func(t *testing.T) {
		response := r.adminRequest("GET", entry.Path(), nil)
		json := response.BodyJSON()
		assert.Equal(t, true, json["locked"])
	})

	t.Run("entry 2 should be locked after being shown in a playlist", func(t *testing.T) {
		response := r.adminRequest("GET", entry2.Path(), nil)
		json := response.BodyJSON()
		assert.Equal(t, true, json["locked"])
	})

	t.Run(`POST "action=show" to playlist slide 2 updates shownAt`, func(t *testing.T) {
		response := r.adminRequest("POST", playlist3.Path()+"/show/2", gin.H{"action": "show"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["shownAt"])
	})

	t.Run(`PATCH playlist with "action=close" closes live voting, but not regular voting`, func(t *testing.T) {
		response := r.adminRequest("PATCH", playlist3.Path(), gin.H{"action": "close"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["liveVotingClosedAt"])
		assert.Nil(t, json["votingClosedAt"])
	})

	t.Run(`PATCH playlist with "action=close" again closes regular voting`, func(t *testing.T) {
		response := r.adminRequest("PATCH", playlist3.Path(), gin.H{"action": "close"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["liveVotingClosedAt"])
		assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["votingClosedAt"])
	})

	t.Run(`PATCH all playlists with "action=close" closes voting for all playlists`, func(t *testing.T) {
		response := r.adminRequest("PATCH", "/playlists", gin.H{"action": "close"})
		assert.Equal(t, http.StatusOK, response.StatusCode)

		for i := 2; i <= 3; i++ {
			path := fmt.Sprintf("/playlists/%d", i)
			t.Run(fmt.Sprintf("GET %s confirms voting is closed", path), func(t *testing.T) {
				response := r.adminRequest("GET", path, nil)
				json := response.BodyJSON()
				assert.Equal(t, http.StatusOK, response.StatusCode)
				assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["liveVotingClosedAt"])
				assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["votingClosedAt"])
			})
		}
	})

	t.Run(`PATCH playlist with "action=open" opens regular voting, but not live voting`, func(t *testing.T) {
		response := r.adminRequest("PATCH", playlist3.Path(), gin.H{"action": "open"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["liveVotingClosedAt"])
		assert.Nil(t, json["votingClosedAt"])
	})

	t.Run(`PATCH playlist with "action=open" again opens live voting`, func(t *testing.T) {
		response := r.adminRequest("PATCH", playlist3.Path(), gin.H{"action": "open"})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Nil(t, json["liveVotingClosedAt"])
		assert.Nil(t, json["votingClosedAt"])
	})
}

func TestPlaylistArchive(t *testing.T) {
	r := NewTestRouterAdmin(t)

	compo := r.createTestCompo()
	entry := r.createTestEntry(compo)
	r.createUpload(entry, "test.txt", []byte("test-upload"))
	playlist := r.createPlaylist("Playlist 1", []uuid.UUID{entry.ID})

	t.Run("GET playlist archive returns expected ZIP file", func(t *testing.T) {
		expectedZip := []byte("PK\x03\x04\x14\x00\b\x00\b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\b\x00\x00\x00test.txt*I-.\xd1--\xc8\xc9OL\x01\x04\x00\x00\xff\xffPK\a\b3 \x93\xfb\x11\x00\x00\x00\v\x00\x00\x00PK\x01\x02\x14\x00\x14\x00\b\x00\b\x00\x00\x00\x00\x003 \x93\xfb\x11\x00\x00\x00\v\x00\x00\x00\b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00test.txtPK\x05\x06\x00\x00\x00\x00\x01\x00\x01\x006\x00\x00\x00G\x00\x00\x00\x00\x00")
		response := r.adminRequest("GET", playlist.ArchivePath(), nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, expectedZip, response.Body)
	})

	t.Run(`GET playlist archive with "ordered=true" returns expected ZIP file`, func(t *testing.T) {
		response := r.adminRequest("GET", playlist.ArchivePath()+"?ordered=true", nil)
		expectedZip := []byte("PK\x03\x04\x14\x00\b\x00\b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1d\x00\x00\x0000_test-author-test-entry.zip\x01\x00\x00\xff\xffPK\a\b\x00\x00\x00\x00\x05\x00\x00\x00\x00\x00\x00\x00PK\x01\x02\x14\x00\x14\x00\b\x00\b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x05\x00\x00\x00\x00\x00\x00\x00\x1d\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0000_test-author-test-entry.zipPK\x05\x06\x00\x00\x00\x00\x01\x00\x01\x00K\x00\x00\x00P\x00\x00\x00\x00\x00")
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, expectedZip, response.Body)
	})
}

func TestPlaylistResults(t *testing.T) {
	r := NewTestRouterAdmin(t)
	r.createVotes()

	t.Run("GET / returns stats", func(t *testing.T) {
		response := r.adminRequest("GET", "/playlists/1/results", nil)
		results := response.BodyJSON()["Results"]
		assert.Equal(t, http.StatusOK, response.StatusCode)

		assert.Equal(t, 5, len(results.([]any)))

		assert.Equal(t, []any{map[string]any{
			"Entry": map[string]any{
				"author":   "handle-1",
				"id":       "/entries/95af5a25-3679-41ba-a2ff-6cd471c483f1",
				"locked":   false,
				"notes":    "notes-1",
				"platform": "platform-1",
				"preview":  map[string]any{"id": "/entries/95af5a25-3679-41ba-a2ff-6cd471c483f1/preview"},
				"title":    "title-1",
			},
			"Score": 5.0,
		}}, results.([]any)[0])

		assert.Equal(t, []any{map[string]any{
			"Entry": map[string]any{
				"author":   "handle-1",
				"id":       "/entries/eb9d18a4-4784-445d-87f3-c67cf22746e9",
				"locked":   false,
				"notes":    "notes-1",
				"platform": "platform-1",
				"preview":  map[string]any{"id": "/entries/eb9d18a4-4784-445d-87f3-c67cf22746e9/preview"},
				"title":    "title-1",
			},
			"Score": 4.0,
		}}, results.([]any)[1])

		assert.Equal(t, []any{map[string]any{
			"Entry": map[string]any{
				"author":   "handle-1",
				"id":       "/entries/6694d2c4-22ac-4208-a007-2939487f6999",
				"locked":   false,
				"notes":    "notes-1",
				"platform": "platform-1",
				"preview":  map[string]any{"id": "/entries/6694d2c4-22ac-4208-a007-2939487f6999/preview"},
				"title":    "title-1",
			},
			"Score": 3.0,
		}}, results.([]any)[2])

		assert.Equal(t, []any{map[string]any{
			"Entry": map[string]any{
				"author":   "handle-1",
				"id":       "/entries/81855ad8-681d-4d86-91e9-1e00167939cb",
				"locked":   false,
				"notes":    "notes-1",
				"platform": "platform-1",
				"preview":  map[string]any{"id": "/entries/81855ad8-681d-4d86-91e9-1e00167939cb/preview"},
				"title":    "title-1",
			},
			"Score": 2.0,
		}}, results.([]any)[3])

		assert.Equal(t, []any{map[string]any{
			"Entry": map[string]any{
				"author":   "handle-1",
				"id":       "/entries/9566c74d-1003-4c4d-bbbb-0407d1e2c649",
				"locked":   false,
				"notes":    "notes-1",
				"platform": "platform-1",
				"preview":  map[string]any{"id": "/entries/9566c74d-1003-4c4d-bbbb-0407d1e2c649/preview"},
				"title":    "title-1",
			},
			"Score": 1.0,
		}}, results.([]any)[4])

	})
}

func TestPlaylistEntryPreview(t *testing.T) {
	r := NewTestRouterAdmin(t)

	compo := r.createTestCompo()
	entry := r.createTestEntry(compo)
	playlist := r.createPlaylist("Playlist 1", []uuid.UUID{entry.ID})

	t.Run("GET playlist entry preview returns expected HTML", func(t *testing.T) {
		response := r.adminRequest("GET", fmt.Sprintf("%v/entries/%v", playlist.Path(), entry.ID), nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), "test-entry</h1>")
	})
}

func TestResults(t *testing.T) {
	r := NewTestRouterAdmin(t)
	r.createVotes()

	t.Run("GET /results returns results", func(t *testing.T) {
		response := r.adminRequest("GET", "/results", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, `
playlist-1
 1.   5 pts  title-1 by handle-1
 2.   4 pts  title-1 by handle-1
 3.   3 pts  title-1 by handle-1
 4.   2 pts  title-1 by handle-1
 5.   1 pts  title-1 by handle-1

playlist-2
 1.   5 pts  title-2 by handle-2
 2.   4 pts  title-2 by handle-2
 3.   3 pts  title-2 by handle-2
 4.   2 pts  title-2 by handle-2
 5.   1 pts  title-2 by handle-2

playlist-3
 1.   5 pts  title-3 by handle-3
 2.   4 pts  title-3 by handle-3
 3.   3 pts  title-3 by handle-3
 4.   2 pts  title-3 by handle-3
 5.   1 pts  title-3 by handle-3

playlist-4
 1.   5 pts  title-4 by handle-4
 2.   4 pts  title-4 by handle-4
 3.   3 pts  title-4 by handle-4
 4.   2 pts  title-4 by handle-4
 5.   1 pts  title-4 by handle-4

playlist-5
 1.   5 pts  title-5 by handle-5
 2.   4 pts  title-5 by handle-5
 3.   3 pts  title-5 by handle-5
 4.   2 pts  title-5 by handle-5
 5.   1 pts  title-5 by handle-5

`, response.BodyString())
	})
}
