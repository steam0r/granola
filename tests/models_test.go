package tests

import (
	"testing"

	"granola/models"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
)

func TestInvite(t *testing.T) {
	r := NewTestRouterUser(t)

	t.Run("new invite with empty key", func(t *testing.T) {
		_, err := models.NewInvite(r.DB, "")

		assert.Error(t, err)
		assert.ErrorContains(t, err, "invites.key")
	})
}

func TestUser(t *testing.T) {
	r := NewTestRouterUser(t)

	t.Run("new user without password", func(t *testing.T) {
		user1, err := models.NewUser(r.DB, "user1@example.com", "handle", nil)
		assert.NoError(t, err)
		assert.Equal(t, "", user1.PasswordHash)

		password := ""
		user2, err := models.NewUser(r.DB, "user2@example.com", "handle", &password)
		assert.NoError(t, err)
		assert.NotEqual(t, "", user2.PasswordHash)
	})

	t.Run("new user with invalid invitation fails", func(t *testing.T) {
		err := r.DB.Transaction(func(tx *gorm.DB) error {
			_, err := models.NewUserWithInvite(tx, "user@example.com", "handle", "password", "invalid-invite")
			return err
		})

		assert.Error(t, err)
		assert.ErrorContains(t, err, "invalid invitation")
	})

	r.createInvite("invite")

	t.Run("new user with empty e-mail fails", func(t *testing.T) {
		err := r.DB.Transaction(func(tx *gorm.DB) error {
			_, err := models.NewUserWithInvite(tx, "", "handle", "password", "invite")
			return err
		})

		assert.Error(t, err)
		assert.ErrorContains(t, err, "users.email")
	})

	t.Run("new user with empty handle fails", func(t *testing.T) {
		err := r.DB.Transaction(func(tx *gorm.DB) error {
			_, err := models.NewUserWithInvite(tx, "user@example.com", "", "password", "invite")
			return err
		})

		assert.Error(t, err)
		assert.ErrorContains(t, err, "users.handle")
	})

	var user models.User

	t.Run("new user works", func(t *testing.T) {
		err := r.DB.Transaction(func(tx *gorm.DB) error {
			user, err := models.NewUserWithInvite(tx, "user@example.com", "handle", "password", "invite")
			assert.NotNil(t, user)
			return err
		})

		assert.NoError(t, err)
	})

	t.Run("User.SetPasswordHash works", func(t *testing.T) {
		assert.Empty(t, user.PasswordHash)

		err := user.SetPassword("password")

		assert.NoError(t, err)
		assert.NotEmpty(t, user.PasswordHash)
	})
}

func TestCompo(t *testing.T) {
	r := NewTestRouterUser(t)

	t.Run("new compo with empty name fails", func(t *testing.T) {
		_, err := models.NewCompo(r.DB, "", false, false, models.CompoTypeUndefined, "")

		assert.Error(t, err)
		assert.ErrorContains(t, err, "compos.name")
	})

	t.Run("new compo works", func(t *testing.T) {
		compo, err := models.NewCompo(r.DB, "compo", false, false, models.CompoTypeUndefined, "")

		assert.NoError(t, err)
		assert.Equal(t, "compo", compo.Name)
	})
}

func TestEntry(t *testing.T) {
	r := NewTestRouterUser(t)

	compo, err := models.NewCompo(r.DB, "test-compo", false, false, models.CompoTypeUndefined, "")
	if err != nil {
		panic(err)
	}

	t.Run("new entry with empty title fails", func(t *testing.T) {
		_, err := models.NewEntry(r.DB, "", "author", "platform", "notes", compo, &models.User{}, false)

		assert.Error(t, err)
		assert.ErrorContains(t, err, "entries.title")
	})

	t.Run("new entry with empty author fails", func(t *testing.T) {
		_, err := models.NewEntry(r.DB, "title", "", "platform", "notes", compo, &models.User{}, false)

		assert.Error(t, err)
		assert.ErrorContains(t, err, "entries.author")
	})

	t.Run("new entry with empty compo fails", func(t *testing.T) {
		_, err := models.NewEntry(r.DB, "title", "author", "platform", "notes", &models.Compo{}, &models.User{}, false)

		assert.Error(t, err)
		assert.ErrorContains(t, err, "entries.compo_id")
	})
}

func TestUpload(t *testing.T) {
	r := NewTestRouterUser(t)

	compo, err := models.NewCompo(r.DB, "test-compo", false, false, models.CompoTypeUndefined, "")
	if err != nil {
		panic(err)
	}

	entry, err := models.NewEntry(r.DB, "test-entry", "author", "platform", "notes", compo, &models.User{}, false)
	if err != nil {
		panic(err)
	}

	t.Run("new upload with no entry ID fails", func(t *testing.T) {
		_, err := models.NewUpload(r.DB, "filename.txt", []byte("content"), &models.Entry{})

		assert.Error(t, err)
		assert.ErrorContains(t, err, "uploads.entry_id")
	})

	t.Run("new upload no empty filename fails", func(t *testing.T) {
		_, err := models.NewUpload(r.DB, "", []byte("content"), entry)

		assert.Error(t, err)
		assert.ErrorContains(t, err, "uploads.filename")
	})

	t.Run("new placeholder upload with no content works", func(t *testing.T) {
		_, err := models.NewUpload(r.DB, "filename.txt", []byte{}, entry)
		assert.NoError(t, err)
	})
}

func TestPlaylistEntry(t *testing.T) {
	r := NewTestRouterUser(t)

	t.Run("new playlist entry with empty entry fails", func(t *testing.T) {
		playlistEntry := models.PlaylistEntry{
			PlaylistID: 1,
		}
		err := r.DB.Save(&playlistEntry).Error

		assert.Error(t, err)
		assert.ErrorContains(t, err, "playlist_entries.entry_id")
	})

	t.Run("new playlist entry with empty playlist fails", func(t *testing.T) {
		playlistEntry := models.PlaylistEntry{
			EntryID: uuid.Nil,
		}
		err := r.DB.Save(&playlistEntry).Error

		assert.Error(t, err)
		assert.ErrorContains(t, err, "playlist_entries.playlist_id")
	})
}

func TestVote(t *testing.T) {
	r := NewTestRouterUser(t)
	compo := r.createTestCompo()
	user := r.createTestUser()
	entry := r.createTestEntry(compo)

	t.Run("new vote with empty entry fails", func(t *testing.T) {
		vote := models.Vote{
			UserID: user.ID,
		}
		err := r.DB.Save(&vote).Error

		assert.Error(t, err)
		assert.ErrorContains(t, err, "votes.entry_id")
	})

	t.Run("new vote with empty user fails", func(t *testing.T) {
		vote := models.Vote{
			EntryID: entry.ID,
		}
		err := r.DB.Save(&vote).Error

		assert.Error(t, err)
		assert.ErrorContains(t, err, "votes.user_id")
	})

	r.createVotes()

	t.Run("get votes", func(t *testing.T) {
		voteTotals, err := models.GetVoteTotals(r.DB)

		assert.NoError(t, err)
		assert.Equal(t, 5, voteTotals.Votes)
		assert.Equal(t, 17, voteTotals.Score)
	})
}
