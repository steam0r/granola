package tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"granola/config"
	"granola/data"
	"granola/models"
	"granola/server"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type TestRouter struct {
	Engine *gin.Engine
	DB     *gorm.DB
}

type TestResponseRecorder struct {
	*httptest.ResponseRecorder
	closeChannel chan bool
}

type TestResponse struct {
	ContentType string
	Location    string
	StatusCode  int
	Body        []byte
	Cookies     []string
}

func (response *TestResponse) BodyString() string {
	return string(response.Body)
}

func (response *TestResponse) BodyJSON() gin.H {
	if !strings.Contains(response.ContentType, "application/json") {
		panic(fmt.Errorf("Content-Type is not JSON: %s", response.ContentType))
	}

	var h gin.H
	if err := json.Unmarshal(response.Body, &h); err != nil {
		s := response.BodyString()
		panic(fmt.Errorf("error converting JSON. %v %s", err, s))
	}

	return h
}

func (r *TestResponseRecorder) CloseNotify() <-chan bool {
	return r.closeChannel
}

func (r *TestResponseRecorder) closeClient() {
	r.closeChannel <- true
}

func CreateTestResponseRecorder() *TestResponseRecorder {
	return &TestResponseRecorder{
		httptest.NewRecorder(),
		make(chan bool, 1),
	}
}

func (r *TestRouter) requestHTML(method string, path string, cookies []string) TestResponse {
	req, _ := http.NewRequest(method, path, nil)
	req.Header.Set("Accept", "text/html")

	for _, cookie := range cookies {
		req.Header.Set("Cookie", cookie)
	}

	w := httptest.NewRecorder()
	r.Engine.ServeHTTP(w, req)
	responseData, _ := io.ReadAll(w.Body)
	return TestResponse{
		StatusCode:  w.Code,
		Body:        responseData,
		Cookies:     w.Header().Values("Set-Cookie"),
		ContentType: w.Header().Get("Content-Type"),
		Location:    w.Header().Get("Location"),
	}
}

func (r *TestRouter) requestData(method string, path string, data []byte, filename string, cookies []string) TestResponse {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	contentType := writer.FormDataContentType()
	part, _ := writer.CreateFormFile("file", filename)
	if _, err := part.Write(data); err != nil {
		panic(err)
	}
	if err := writer.Close(); err != nil {
		panic(err)
	}
	return r.request(method, path, contentType, body, cookies)
}

func (r *TestRouter) requestJson(method string, path string, obj gin.H, cookies []string) TestResponse {
	d, _ := json.Marshal(obj)
	body := bytes.NewBuffer(d)
	contentType := "application/json; charset=UTF-8"
	return r.request(method, path, contentType, body, cookies)
}

func (r *TestRouter) request(method string, path string, contentType string, body io.Reader, cookies []string) TestResponse {
	req, _ := http.NewRequest(method, path, body)
	req.Header.Set("Content-Type", contentType)

	for _, cookie := range cookies {
		req.Header.Set("Cookie", cookie)
	}

	w := CreateTestResponseRecorder()
	r.Engine.ServeHTTP(w, req)

	responseData, _ := io.ReadAll(w.Body)
	return TestResponse{
		StatusCode:  w.Code,
		Body:        responseData,
		Cookies:     w.Header().Values("Set-Cookie"),
		ContentType: w.Header().Get("Content-Type"),
		Location:    w.Header().Get("Location"),
	}
}

func setupTestConfig() config.Config {
	cfg := config.Config{
		Secret:       []byte("not-very-secret"),
		DatabasePath: ":memory:",
		Admins:       map[string]string{"admin": "password"},
	}

	cfg.Routing.BaseURL = "http://test-host"
	cfg.Routing.MinimumPasswordLength = 5
	cfg.RateLimit.Register = ^uint32(0)
	cfg.RateLimit.Login = ^uint32(0)

	if !testing.Verbose() {
		cfg.Logging.Writer = io.Discard
		cfg.Logging.Level = config.Silent
	}

	return cfg
}

func openTestDatabase(t *testing.T, config config.Config) *gorm.DB {
	db := data.OpenDatabase(config.DatabasePath, config.Logging)

	t.Cleanup(func() {
		sql, _ := db.DB()
		if err := sql.Close(); err != nil {
			log.Print(err)
		}
		db = nil
	})

	return db
}

func NewTestRouterUser(t *testing.T) TestRouter {
	uuid.SetRand(rand.New(rand.NewSource(1)))
	gin.SetMode(gin.TestMode)

	c := setupTestConfig()
	db := openTestDatabase(t, c)
	s := server.NewServer(c, db)

	s.Engine.Use(s.UserHandler.CheckAuth)
	s.UserHandler.RegisterRoutes(s.Engine)

	return TestRouter{
		Engine: s.Engine,
		DB:     db,
	}
}

func NewTestRouterAdmin(t *testing.T) TestRouter {
	uuid.SetRand(rand.New(rand.NewSource(1)))
	gin.SetMode(gin.TestMode)

	c := setupTestConfig()
	db := openTestDatabase(t, c)
	s := server.NewServer(c, db)

	s.AdminHandler.RegisterRoutes(s.Engine, c.Admins)

	return TestRouter{
		Engine: s.Engine,
		DB:     db,
	}
}

func (r *TestRouter) createUser(email string, handle string, password string, invite string) *models.User {
	var user *models.User
	var err error
	if err := r.DB.Transaction(func(tx *gorm.DB) error {
		user, err = models.NewUserWithInvite(tx, email, handle, password, invite)
		return err
	}); err != nil {
		panic(err)
	}

	return user
}

func (r *TestRouter) loginUser(email string, password string) []string {
	obj := gin.H{
		"email":    email,
		"password": password,
	}
	response := r.requestJson("POST", "/login", obj, nil)
	if response.StatusCode != http.StatusOK {
		panic("failed to log in")
	}
	return response.Cookies
}

func (r *TestRouter) adminRequest(method string, path string, obj gin.H) TestResponse {
	d, _ := json.Marshal(obj)
	body := bytes.NewBuffer(d)
	req, _ := http.NewRequest(method, path, body)
	req.SetBasicAuth("admin", "password")
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	w := CreateTestResponseRecorder()
	r.Engine.ServeHTTP(w, req)

	responseData, _ := io.ReadAll(w.Body)
	return TestResponse{
		StatusCode:  w.Code,
		Body:        responseData,
		ContentType: w.Header().Get("Content-Type"),
		Location:    w.Header().Get("Location"),
	}
}

func (r *TestRouter) adminRequestHTML(method string, path string) TestResponse {
	req, _ := http.NewRequest(method, path, nil)
	req.SetBasicAuth("admin", "password")
	req.Header.Set("Accept", "text/html")
	w := httptest.NewRecorder()
	r.Engine.ServeHTTP(w, req)

	responseData, _ := io.ReadAll(w.Body)
	return TestResponse{
		StatusCode:  w.Code,
		Body:        responseData,
		ContentType: w.Header().Get("Content-Type"),
		Location:    w.Header().Get("Location"),
	}
}

func (r *TestRouter) adminPostFormData(path string, data map[string]string) TestResponse {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	for k, v := range data {
		part, err := writer.CreateFormField(k)
		if err != nil {
			panic(err)
		}
		part.Write([]byte(v))
	}
	writer.Close()

	req, _ := http.NewRequest("POST", path, body)
	req.SetBasicAuth("admin", "password")
	req.Header.Set("Content-Type", writer.FormDataContentType())
	w := CreateTestResponseRecorder()
	r.Engine.ServeHTTP(w, req)

	responseData, _ := io.ReadAll(w.Body)
	return TestResponse{
		StatusCode:  w.Code,
		Body:        responseData,
		ContentType: w.Header().Get("Content-Type"),
		Location:    w.Header().Get("Location"),
	}
}

func (r *TestRouter) createCompo(name string, multiPlatform bool, locked bool, ct models.CompoType, description string) *models.Compo {
	compo, err := models.NewCompo(r.DB, name, multiPlatform, locked, ct, description)
	if err != nil {
		panic(err)
	}
	return compo
}

func (r *TestRouter) createTestCompo() *models.Compo {
	return r.createCompo("test compo", true, false, models.CompoTypeUndefined, "test description")
}

func (r *TestRouter) createInvite(key string) {
	_, err := models.NewInvite(r.DB, key)
	if err != nil {
		panic(err)
	}
}

func (r *TestRouter) createTestUser() *models.User {
	r.createInvite("test-invite")
	return r.createUser("test-user@example.com", "test.user", "test-password", "test-invite")
}

func (r *TestRouter) lockEntry(entry *models.Entry) {
	entry.Locked = true

	result := r.DB.Save(&entry)
	if result.Error != nil {
		panic(result.Error)
	}
}

func (r *TestRouter) createUserEntry(title string, author string, platform string, notes string, compo *models.Compo, user *models.User) *models.Entry {
	entry, err := models.NewEntry(r.DB, title, author, platform, notes, compo, user, false)
	if err != nil {
		panic(err)
	}
	return entry
}

func (r *TestRouter) createEntry(compo *models.Compo, title string, author string, platform string, notes string) *models.Entry {
	return r.createUserEntry(title, author, platform, notes, compo, &models.User{})
}

func (r *TestRouter) createUserTestEntry(compo *models.Compo, user *models.User) *models.Entry {
	return r.createUserEntry("test-entry", "test-author", "test-platform", "test-notes", compo, user)
}

func (r *TestRouter) createTestEntry(compo *models.Compo) *models.Entry {
	return r.createEntry(compo, "test-entry", "test-author", "test-platform", "test-notes")
}

func (r *TestRouter) createUpload(entry *models.Entry, filename string, data []byte) *models.Upload {
	upload, err := models.NewUpload(r.DB, filename, data, entry)
	if err != nil {
		panic(err)
	}
	return upload
}

func (r *TestRouter) createPlaylist(name string, entries []uuid.UUID) *models.Playlist {
	playlist, err := models.NewPlaylist(r.DB, name, entries)
	if err != nil {
		panic(err)
	}

	return playlist
}

func (r *TestRouter) createVotes() {
	err := r.DB.Transaction(func(tx *gorm.DB) error {
		for i := 1; i <= 5; i++ {
			compo, err := models.NewCompo(tx,
				fmt.Sprintf("compo-%d", i),
				false,
				false,
				models.CompoTypeUndefined,
				"")
			if err != nil {
				return err
			}

			password := fmt.Sprintf("password-%d", i)
			user, err := models.NewUser(tx,
				fmt.Sprintf("user-%d@example.com", i),
				fmt.Sprintf("handle-%d", i),
				&password)
			if err != nil {
				return err
			}

			playlist := &models.Playlist{Name: fmt.Sprintf("playlist-%d", i)}
			if err = tx.Save(playlist).Error; err != nil {
				return err
			}

			for j := 1; j <= 5; j++ {
				entry, err := models.NewEntry(tx,
					fmt.Sprintf("title-%d", i),
					user.Handle,
					fmt.Sprintf("platform-%d", i),
					fmt.Sprintf("notes-%d", i),
					compo,
					user, false)

				if err != nil {
					return err
				}

				playlistEntry := &models.PlaylistEntry{
					PlaylistID: playlist.ID,
					EntryID:    entry.ID,
				}
				if err = tx.Save(playlistEntry).Error; err != nil {
					return err
				}

				for k := 1; k <= j; k++ {
					score := uint(k)
					if i%2 == 0 {
						// Every other user should give a bad score as its latest vote
						score = 5 - uint(k) + 1
					}
					vote := &models.Vote{
						EntryID: entry.ID,
						UserID:  user.ID,
						Score:   score,
					}
					if err = tx.Save(vote).Error; err != nil {
						return err
					}
				}
			}
		}

		return nil
	})
	if err != nil {
		panic(err)
	}
}
