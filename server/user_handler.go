package server

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"sync/atomic"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"granola/models"
)

type UserHandler struct {
	BaseHandler
	playlistStreamer PlaylistStreamer
}

func getUser(c *gin.Context) models.User {
	return c.MustGet("user").(models.User)
}

func requireAuth(c *gin.Context) {
	user, _ := c.Get("user")
	if user == nil {
		c.Redirect(http.StatusTemporaryRedirect, "/login")
		c.Abort()
		return
	}
	c.Next()
}

func (h *UserHandler) CheckAuth(c *gin.Context) {
	session := sessions.Default(c)
	if sessionUser := session.Get("user"); sessionUser != nil {
		user := models.User{}
		result := h.DB.First(&user, sessionUser)
		if result.Error != nil {
			session.Delete("user")
			if err := session.Save(); err != nil {
				panic(err)
			}
		} else {
			c.Set("user", user)
		}
	}
	c.Next()
}

func (h *UserHandler) getEntries(c *gin.Context) {
	var entries []models.Entry
	user := getUser(c)
	result := h.DB.
		Preload("Compo").
		Preload("User").
		Preload("Uploads", func(tx *gorm.DB) *gorm.DB { return tx.Omit("Content") }).
		Where("user_id = ?", user.ID).
		Find(&entries)

	if result.Error != nil {
		panic(result.Error)
	}

	var compos []models.Compo
	result = h.DB.Where("locked = false").Find(&compos)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Entries",
		"Entries":     entries,
		"Compos":      compos,
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "user/entries",
		HTMLData: &data,
		Data:     entries,
	})
}

func (h *UserHandler) getCompo(c *gin.Context) {
	compo := models.Compo{}
	if err := h.DB.
		Where("locked = false").
		First(&compo, c.Param("compo_id")).
		Error; err != nil {
		panic(err)
	}

	data := gin.H{
		"Config":      h.Config.Routing,
		"Compo":       &compo,
		"CompoTypes":  models.CompoTypes,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "user/compo",
		HTMLData: &data,
		Data:     &compo,
	})
}

func (h *UserHandler) postEntry(c *gin.Context) {
	args := struct {
		Title    string `json:"title" form:"title" binding:"required"`
		Author   string `json:"author" form:"author" binding:"required"`
		Platform string `json:"platform" form:"platform"`
		Notes    string `json:"notes" form:"notes"`
		CompoID  int    `json:"compo" form:"compo" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	compo := models.Compo{}
	result := h.DB.Where("locked = false").First(&compo, args.CompoID)
	if result.Error != nil {
		panic(fmt.Errorf("compo %d not found", args.CompoID))
	}

	user := getUser(c)

	entry, err := models.NewEntry(h.DB, args.Title, args.Author, args.Platform, args.Notes, &compo, &user, false)
	if err != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, "could not create entry")
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, entry.Path())
	} else {
		c.JSON(http.StatusCreated, &entry)
	}
}

func (h *UserHandler) getEntry(c *gin.Context) {
	user := getUser(c)
	entry := models.Entry{}
	result := h.DB.
		Preload("Uploads", func(tx *gorm.DB) *gorm.DB { return tx.Omit("Content") }).
		Preload("Compo").
		Preload("User").
		Preload("Uploads.Entry").
		Where("user_id = ?", user.ID).
		First(&entry, "id = ?", c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Entry",
		"Entry":       &entry,
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "user/entry",
		HTMLData: &data,
		Data:     &entry,
	})
}

func (h *UserHandler) getEntryPreview(c *gin.Context) {
	user := getUser(c)
	entry := models.Entry{}
	result := h.DB.
		Preload("Compo").
		Preload("User").
		Preload("Uploads.Entry").
		Where("user_id = ?", user.ID).
		First(&entry, "id = ?", c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Entry Preview",
		"Entry":       &entry,
		"EntryNumber": 0,
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.HTML(http.StatusOK, "entry-preview", data)
}

func (h *UserHandler) getPlaylistEntryPreview(c *gin.Context) {
	playlistEntry := models.PlaylistEntry{}
	if err := h.DB.
		Preload("Playlist").
		Preload("Entry.User").
		Preload("Entry.Uploads").
		Where("playlist_id = ?", c.Param("playlist_id")).
		Where("entry_id = ?", c.Param("entry_id")).
		Where("shown_at <= ?", time.Now()).
		First(&playlistEntry).
		Error; err != nil {
		panic(err)
	}

	var user *models.User = nil
	tmp, exists := c.Get("user")
	if exists {
		tmpUser := tmp.(models.User)
		user = &tmpUser
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Playlist Preview",
		"Entry":       &playlistEntry.Entry,
		"EntryNumber": playlistEntry.Order,
		"Playlist":    &playlistEntry.Playlist,
		"User":        user,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.HTML(http.StatusOK, "entry-preview", data)
}

func (h *UserHandler) putEntry(c *gin.Context) {
	entry := models.Entry{}
	result := h.DB.
		Preload("Uploads", func(tx *gorm.DB) *gorm.DB { return tx.Omit("Content") }).
		Preload("Compo").
		Preload("User").
		Where("user_id = ? AND locked = false", getUser(c).ID).
		First(&entry, "id = ?", c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	args := struct {
		Title    string `json:"title" form:"title" binding:"required"`
		Author   string `json:"author" form:"author" binding:"required"`
		Platform string `json:"platform" form:"platform"`
		Notes    string `json:"notes" form:"notes"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	entry.Title = args.Title
	entry.Author = args.Author
	entry.Platform = args.Platform
	entry.Notes = args.Notes

	result = h.DB.Omit("Locked").Save(&entry)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, entry.Path())
	} else {
		c.JSON(http.StatusOK, &entry)
	}
}

func (h *UserHandler) deleteEntry(c *gin.Context) {
	result := h.DB.
		Where("user_id = ? AND locked = false", getUser(c).ID).
		Delete(&models.Entry{}, "id = ?", c.Param("entry_id"))

	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "/entries")
	} else {
		c.Status(http.StatusNoContent)
	}
}

func (h *UserHandler) postVote(c *gin.Context) {
	args := struct {
		Score uint `json:"score" form:"score" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	user := getUser(c)
	playlistEntry := models.PlaylistEntry{}
	if err := h.DB.
		Preload("Entry").
		Where("playlist_id = ?", c.Param("playlist_id")).
		Where("entry_id = ?", c.Param("entry_id")).
		First(&playlistEntry).
		Error; err != nil {
		panic(err)
	}

	vote := models.Vote{Entry: playlistEntry.Entry, User: user, Score: args.Score}
	err := h.DB.Create(&vote).Error
	if err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "/voting")
	} else {
		c.JSON(http.StatusCreated, &vote)
	}
}

func (h *UserHandler) showRegister(c *gin.Context) {
	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Register",
		"Invite":      c.Query("invite"),
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.HTML(http.StatusOK, "user/register", data)
}

func (h *UserHandler) postUser(c *gin.Context) {
	args := struct {
		Email    string `json:"email" form:"email" binding:"required,email"`
		Handle   string `json:"handle" form:"handle" binding:"required"`
		Invite   string `json:"invite" form:"invite" binding:"required"`
		Password string `json:"password" form:"password" binding:"required,password"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	var user *models.User
	var err error
	if err := h.DB.Transaction(func(tx *gorm.DB) error {
		user, err = models.NewUserWithInvite(tx, args.Email, args.Handle, args.Password, args.Invite)
		return err
	}); err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		session := sessions.Default(c)
		session.Set("user", user.ID)
		if err := session.Save(); err != nil {
			panic(err)
		}
		c.Redirect(http.StatusFound, "/")
	} else {
		c.JSON(http.StatusCreated, &user)
	}
}

func (h *UserHandler) showLogin(c *gin.Context) {
	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Login",
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.HTML(http.StatusOK, "user/login", data)
}

func (h *UserHandler) postLogin(c *gin.Context) {
	session := sessions.Default(c)

	args := struct {
		Email    string `json:"email" form:"email" binding:"required"`
		Password string `json:"password" form:"password" binding:"required"`
	}{}

	if err := c.ShouldBind(&args); err != nil {
		h.abortWithStatusAndText(c, http.StatusUnauthorized, err.Error())
		return
	}

	user := models.User{}
	if err := h.DB.First(&user, "email = ?", args.Email).Error; err != nil {
		log.Printf("No user found for %s. %v", args.Email, err)
	}
	if !user.CheckPassword(args.Password) {
		message := "Invalid credentials"
		htmlData := gin.H{
			"Config":      &h.Config.Routing,
			"Title":       "Login",
			"Email":       args.Email,
			"Error":       message,
			"HttpContext": c.MustGet("HttpContext"),
		}
		jsonData := gin.H{"detail": message}
		c.Negotiate(http.StatusUnauthorized, gin.Negotiate{
			Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
			HTMLName: "user/login",
			HTMLData: &htmlData,
			Data:     &jsonData,
		})
		return
	}

	session.Set("user", user.ID)

	if err := session.Save(); err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/")
	} else {
		// It is stupid to use RFC 7807 as a format for a successful
		// response, but I have no good ideas for a better format.
		c.JSON(http.StatusOK, gin.H{"detail": "Successfully authenticated user"})
	}
}

func (h *UserHandler) logoutUser(c *gin.Context) {
	session := sessions.Default(c)
	session.Delete("user")
	if err := session.Save(); err != nil {
		panic(err)
	}
	c.Redirect(http.StatusFound, "/")
}

func (h *UserHandler) getProfile(c *gin.Context) {
	user := getUser(c)
	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Profile",
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "user/profile",
		HTMLData: &data,
		Data:     &user,
	})
}

func (h *UserHandler) putProfile(c *gin.Context) {
	args := struct {
		Email    string `json:"email" form:"email" binding:"required,email"`
		Handle   string `json:"handle" form:"handle" binding:"required"`
		Password string `json:"password" form:"password"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	user := getUser(c)
	user.Email = args.Email
	user.Handle = args.Handle
	if args.Password != "" {
		if err := user.SetPassword(args.Password); err != nil {
			panic(err)
		}
	}
	if result := h.DB.Save(&user); result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/profile")
	} else {
		c.JSON(http.StatusOK, &user)
	}
}

func (h *UserHandler) postUpload(c *gin.Context) {
	formFile, err := c.FormFile("file")
	if err != nil {
		panic(errors.New("file not found in request"))
	}
	file, err := formFile.Open()
	if err != nil {
		panic(err)
	}

	content, err := io.ReadAll(file)
	if err != nil {
		panic(err)
	}

	entry := &models.Entry{}
	result := h.DB.
		Preload("Uploads", func(tx *gorm.DB) *gorm.DB { return tx.Omit("Content") }).
		Where("user_id = ?", getUser(c).ID).
		First(&entry, "id = ?", c.Param("entry_id"))

	if result.Error != nil {
		panic(result.Error)
	}

	if entry.Locked {
		h.abortWithStatusAndText(c, http.StatusMethodNotAllowed, "Entry has been locked")
		return
	}

	upload, err := models.NewUpload(h.DB, formFile.Filename, content, entry)
	if err != nil {
		panic(err)
	}

	// It would have been preferrable to have a reference from Upload to Entry
	// so we could do upload.Entry.CompoID to find the CompoID for the URL, but
	// that somehow made all fields within Upload.Entry required when
	// data-binding the new Upload.
	result = h.DB.First(&upload.Entry, "id = ?", upload.EntryID)
	if result.Error != nil {
		panic(result.Error)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, upload.Path())
	} else {
		c.JSON(http.StatusCreated, &upload)
	}
}

func (h *UserHandler) getUpload(c *gin.Context) {
	user := getUser(c)
	upload := models.Upload{}
	result := h.DB.Omit("Content").
		Preload("Entry").
		Preload("Entry.Compo").
		Joins("JOIN entries on entries.id = uploads.entry_id").
		Where("entry_id = ?", c.Param("entry_id")).
		Where("entries.user_id = ?", user.ID).
		First(&upload, "uploads.id = ?", c.Param("upload_id"))

	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Upload",
		"Upload":      &upload,
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "user/upload",
		HTMLData: &data,
		Data:     &upload,
	})
}

func (h *UserHandler) getUploadData(c *gin.Context) {
	upload := models.Upload{}
	result := h.DB.Joins("JOIN entries on entries.id = uploads.entry_id").
		Where("entries.user_id = ?", getUser(c).ID).
		First(&upload, "uploads.id = ?", c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}
	c.Header("Content-Disposition", "attachment; filename="+upload.Filename)
	c.Data(http.StatusOK, "application/octet-stream", upload.Content)
}

func (h *UserHandler) deleteUpload(c *gin.Context) {
	upload := models.Upload{}
	result := h.DB.Joins("JOIN entries on entries.id = uploads.entry_id").
		Where("entries.user_id = ?", getUser(c).ID).
		First(&upload, "uploads.id = ?", c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	result = h.DB.Delete(&upload)
	if result.Error != nil {
		panic(result.Error)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.Status(http.StatusNoContent)
	}
}

func (h *UserHandler) getVoting(c *gin.Context) {
	user := getUser(c)

	playlist, err := h.currentVotingPlaylist(&user, true)
	if err != nil {
		playlist = nil
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			panic(err)
		}
	}

	var playlists []models.Playlist
	if err := h.DB.
		Where("shown_at is not null").
		Where("live_voting_closed_at is not null").
		Find(&playlists).
		Error; err != nil {
		panic(err)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Playlist":    playlist,
		"Playlists":   playlists,
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}

	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "user/voting/live",
		HTMLData: &data,
		Data:     playlist,
	})
}

func (h *UserHandler) getPlaylistVoting(c *gin.Context) {
	user := getUser(c)

	var playlist *models.Playlist
	if err := h.DB.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry.Uploads").
		Preload("Entries.Playlist").
		First(&playlist, c.Param("playlist_id")).
		Error; err != nil {
		panic(err)
	}

	entries := playlist.EntryIDs()
	votes, err := user.Votes(h.DB, entries)

	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		panic(err)
	}

	votingPlaylist := playlist.VotingPlaylist(votes)

	var playlists []models.Playlist
	if err := h.DB.
		Where("shown_at is not null").
		Where("live_voting_closed_at is not null").
		Find(&playlists).
		Error; err != nil {
		panic(err)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Playlist":    votingPlaylist,
		"Playlists":   playlists,
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}

	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "user/voting/playlist",
		HTMLData: &data,
		Data:     playlist,
	})
}

func (h *UserHandler) currentVotingPlaylist(user *models.User, reverse bool) (*models.VotingPlaylist, error) {
	order := "asc"
	if reverse {
		order = "desc"
	}

	var playlist *models.Playlist
	result := h.DB.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Where("shown_at is not null").Order(fmt.Sprintf("`order` %s", order))
		}).
		Preload("Entries.Playlist").
		Preload("Entries.Entry").
		Preload("Entries.Entry.Compo").
		Preload("Entries.Entry.User").
		Order("shown_at desc").
		Where("shown_at is not null").
		First(&playlist)

	entries := playlist.EntryIDs()
	votes, err := user.Votes(h.DB, entries)

	if err != nil {
		return nil, err
	}

	return playlist.VotingPlaylist(votes), result.Error
}

func (h *UserHandler) currentVotingPlaylistEntry() (*models.PlaylistEntry, error) {
	playlist, err := h.currentVotingPlaylist(nil, false)
	if playlist != nil && len(playlist.Entries) > 0 {
		for _, entry := range playlist.Entries {
			if entry.IsCurrent {
				return &entry.PlaylistEntry, nil
			}
		}
	}

	return nil, err
}

func (h *UserHandler) streamEntries() {
	ticker := time.NewTicker(500 * time.Millisecond)
	var currentEntry *models.PlaylistEntry = nil

	for {
		select {
		case client := <-h.playlistStreamer.NewClient:
			h.playlistStreamer.newClient(client)
			if currentEntry != nil {
				client <- currentEntry
			}

		case client := <-h.playlistStreamer.ClosedClient:
			h.playlistStreamer.closeClient(client)

		case <-ticker.C:
			if len(h.playlistStreamer.ActiveClients) > 0 {
				if entry, err := h.currentVotingPlaylistEntry(); err != nil {
					log.Printf("Error selecting playlist entry for voting: %v", err)
					break
				} else if !currentEntry.Equal(entry) {
					currentEntry = entry
					h.playlistStreamer.broadcast(currentEntry)
				}
			}
		}
	}
}

func (h *UserHandler) getEntryStream(c *gin.Context) {
	c.Writer.Header().Set("Content-Type", "text/event-stream")
	c.Writer.Header().Set("Cache-Control", "no-cache")
	c.Writer.Header().Set("Connection", "keep-alive")
	c.Writer.Header().Set("Transfer-Encoding", "chunked")

	user := getUser(c)
	clientChan := make(PlaylistStreamChan)
	h.playlistStreamer.NewClient <- clientChan
	defer func() {
		h.playlistStreamer.ClosedClient <- clientChan
	}()

	c.Stream(func(w io.Writer) bool {
		if playlistEntry, ok := <-clientChan; ok {
			vote, _ := user.CurrentVote(h.DB, playlistEntry.EntryID)
			votingEntry := models.VotingEntry{PlaylistEntry: *playlistEntry, Vote: vote}
			if bytes, err := votingEntry.MarshalJSON(); err != nil {
				log.Printf("Error converting Entry '%v' to JSON: %v", playlistEntry.EntryID, err)
			} else {
				message := string(bytes)
				c.SSEvent("message", message)
				return true
			}
		}

		return false
	})
}

func (h *UserHandler) registerPrivateRoutes(root *gin.RouterGroup) {
	private := root.Group("/")
	private.Use(requireAuth)
	private.POST("/logout", h.logoutUser)
	private.GET("/compos/:compo_id", h.getCompo)

	profile := private.Group("/profile")
	profile.GET("", h.getProfile)
	profile.PUT("", h.putProfile)

	entries := private.Group("/entries")
	entries.GET("", h.getEntries)
	entries.POST("", h.postEntry)

	entry := entries.Group("/:entry_id")
	entry.GET("", h.getEntry)
	entry.GET("/preview", h.getEntryPreview)
	entry.PUT("", h.putEntry)
	entry.DELETE("", h.deleteEntry)

	playlistEntry := root.Group("/playlists/:playlist_id/entries/:entry_id")
	playlistEntry.GET("", h.getPlaylistEntryPreview)
	playlistEntry.POST("/votes", h.postVote, requireAuth)

	uploads := entry.Group("/uploads")
	uploads.POST("", h.postUpload)

	upload := uploads.Group("/:upload_id")
	upload.GET("", h.getUpload)
	upload.DELETE("", h.deleteUpload)
	upload.GET("/data", h.getUploadData)

	voting := private.Group("/voting")
	voting.GET("", h.getVoting)
	voting.GET("/:playlist_id", h.getPlaylistVoting)
	voting.GET("/entries", h.getEntryStream)
}

func (h *UserHandler) showHome(c *gin.Context) {
	var user *models.User
	tmp, _ := c.Get("user")
	if tmp2, ok := tmp.(models.User); ok {
		user = &tmp2
		if err := h.DB.Where("user_id = ?", user.ID).Find(&user.Entries).Error; err != nil {
			panic(err)
		}
	}

	var compos []models.Compo
	if err := h.DB.Where("locked = false").Find(&compos).Error; err != nil {
		panic(err)
	}

	data := gin.H{
		"Config": &h.Config.Routing,
		"Title":  "Home",
		"User":   user,
		"Compos": compos,
	}
	c.HTML(http.StatusOK, "user/home", data)
}

func rateLimit(max uint32) gin.HandlerFunc {
	var count uint32
	return func(c *gin.Context) {
		curr := atomic.AddUint32(&count, 1)
		defer atomic.AddUint32(&count, ^uint32(0))

		if curr <= max {
			c.Next()
		} else {
			c.Header("Retry-After", "1")
			c.Status(http.StatusTooManyRequests)
		}
	}
}

func (h *UserHandler) getSlideStream(c *gin.Context) {
	c.Writer.Header().Set("Content-Type", "text/event-stream")
	c.Writer.Header().Set("Cache-Control", "no-cache")
	c.Writer.Header().Set("Connection", "keep-alive")
	c.Writer.Header().Set("Transfer-Encoding", "chunked")

	clientChan := make(PlaylistStreamChan)
	h.playlistStreamer.NewClient <- clientChan
	defer func() {
		h.playlistStreamer.ClosedClient <- clientChan
	}()

	c.Stream(func(w io.Writer) bool {
		if playlistEntry, ok := <-clientChan; ok {
			if bytes, err := playlistEntry.MarshalJSON(); err != nil {
				log.Printf("Error converting Entry '%v' to JSON: %v", playlistEntry.EntryID, err)
			} else {
				message := string(bytes)
				c.SSEvent("message", message)
				return true
			}
		}

		return false
	})
}

func (h *UserHandler) RegisterRoutes(r *gin.Engine) {
	root := r.Group("/")
	root.GET("", h.showHome)
	root.GET("tribute", func(c *gin.Context) {
		c.Redirect(http.StatusPermanentRedirect, "http://www.slengpung.com/?id=7623")
	})

	root.GET("slide-stream", h.getSlideStream)

	register := root.Group("/register")
	register.GET("", h.showRegister)
	register.POST("", h.postUser, rateLimit(h.Config.RateLimit.Register))

	login := root.Group("/login")
	login.GET("", h.showLogin)
	login.POST("", h.postLogin, rateLimit(h.Config.RateLimit.Login))

	h.registerPrivateRoutes(root)
}
