package server

import (
	"errors"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"

	"granola/config"
)

type BaseHandler struct {
	Translator ut.Translator
	DB         *gorm.DB
	Config     config.Config
}

func (h *BaseHandler) abortWithStatusAndText(c *gin.Context, status int, errorText string) {
	htmlData := gin.H{
		"Config": &h.Config.Routing,
		"Title":  "Error",
		"Error":  errorText,
		"Status": status,
	}
	jsonData := gin.H{
		"detail": errorText,
	}
	c.Negotiate(status, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "error",
		HTMLData: &htmlData,
		Data:     &jsonData,
	})
	c.Abort()
	log.Print(errorText)
}

func (h *BaseHandler) recovery(c *gin.Context, a any) {
	if validationErrors, ok := a.(validator.ValidationErrors); ok {
		text := validationErrors[0].Translate(h.Translator)
		h.abortWithStatusAndText(c, http.StatusBadRequest, text)
	} else if err, ok := a.(error); ok {
		switch {
		case errors.Is(err, gorm.ErrRecordNotFound):
			h.abortWithStatusAndText(c, http.StatusNotFound, "File not found!")
		default:
			h.abortWithStatusAndText(c, http.StatusBadRequest, err.Error())
		}
	} else {
		c.AbortWithStatus(http.StatusInternalServerError)
	}
}

func (h *BaseHandler) show404(c *gin.Context) {
	h.abortWithStatusAndText(c, http.StatusNotFound, "File not found!")
}
