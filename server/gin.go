package server

import (
	"encoding/base64"
	"encoding/gob"
	"fmt"
	"html/template"
	"io"
	"log"
	"os"
	"path/filepath"
	"runtime/debug"
	"strings"
	"sync"
	"time"

	method "github.com/bu/gin-method-override"
	"github.com/dustin/go-humanize"
	"github.com/gin-contrib/multitemplate"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	translations "github.com/go-playground/validator/v10/translations/en"
	"github.com/google/uuid"
	"github.com/spf13/cast"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

var (
	ValidatorMutex = sync.Mutex{}
)

type Validator struct {
	Func    validator.Func
	Message string
}

func createRender(basedir string) multitemplate.Renderer {
	r := multitemplate.NewRenderer()

	addTemplate := func(templateName string, templates ...string) {
		for i, t := range templates {
			if strings.HasPrefix(t, "templates/") {
				abspath, _ := filepath.Abs(filepath.Join(basedir, t))
				templates[i] = abspath
			}
		}

		r.AddFromFilesFuncs(templateName, template.FuncMap{
			"granolaVersion": func() string {
				info, _ := debug.ReadBuildInfo()
				return info.Main.Version
			},
			"templateName": func() string { return templateName },
			"add":          func(a int, b int) int { return a + b },
			"sub":          func(a int, b int) int { return a - b },
			"int":          func(a any) int { return cast.ToInt(a) },
			"iterate": func(from uint, to uint) []uint {
				var items []uint
				if from > to {
					for i := from; i >= to; i-- {
						items = append(items, i)
					}
				} else {
					for i := from; i <= to; i++ {
						items = append(items, i)
					}
				}

				return items
			},
			"humanize": func(what string, value any) any {
				if value != nil {
					switch what {
					case "time":
						return humanize.Time(value.(time.Time))
					case "bytes":
						return humanize.IBytes(uint64(value.(int)))
					}
				}

				return value
			},
			"title": func(value any) string {
				caser := cases.Title(language.English)
				s := fmt.Sprintf("%v", value)
				return caser.String(s)
			},
			"hasPrefix": func(s string, prefix string) bool {
				return strings.HasPrefix(s, prefix)
			},
			"base64": base64.StdEncoding.EncodeToString,
		}, templates...)
	}

	addTemplates := func(pattern string, requiredTemplates ...string) {
		matches, err := filepath.Glob(filepath.Join(basedir, pattern))
		if err != nil {
			panic(err)
		}

		for _, f := range matches {
			if strings.HasPrefix(filepath.Base(f), "_") {
				continue
			}

			templateName := strings.TrimSuffix(strings.TrimPrefix(f, basedir+"/templates/"), ".html")
			baseTemplate := "templates/_base.html"
			requiredTemplates = append(requiredTemplates, baseTemplate)
			requiredTemplates = append(requiredTemplates, f)
			addTemplate(templateName, requiredTemplates...)
		}
	}

	addTemplate("error", "templates/_base.html", "templates/error.html")
	addTemplate("entry-preview", "templates/entry-preview.html")
	addTemplate("prizegiving-preview", "templates/prizegiving-preview.html")
	addTemplates("templates/user/*.html",
		"templates/user/_base.html",
		"templates/user/_compos.html")
	addTemplates("templates/user/voting/*.html",
		"templates/user/_base.html",
		"templates/user/voting/_checkout.html",
		"templates/user/voting/_current-vote.html",
		"templates/user/voting/_entry-frame.html",
		"templates/user/voting/_place-vote.html",
		"templates/user/voting/_playlist-navigation.html")
	addTemplates("templates/admin/*.html", "templates/admin/_base.html")
	addTemplate("admin/_results", "templates/admin/_results.txt")

	return r
}

func configureStaticFiles(basedir string, r *gin.Engine) {
	abspath, _ := filepath.Abs(filepath.Join(basedir, "static"))
	files, err := os.ReadDir(abspath)
	if err != nil {
		panic(err)
	}

	for _, f := range files {
		if f.IsDir() {
			r.Static("/"+f.Name(), "./static/"+f.Name())
		} else {
			r.StaticFile("/"+f.Name(), "./static/"+f.Name())
		}
	}
}

func configureValidation(validators map[string]Validator) ut.Translator {
	ValidatorMutex.Lock()
	defer ValidatorMutex.Unlock()

	if validation, ok := binding.Validator.Engine().(*validator.Validate); ok {
		englishLocaleTranslator := en.New()
		universalTranslator := ut.New(englishLocaleTranslator, englishLocaleTranslator)
		// this is usually known or extracted from http 'Accept-Language' header
		// also see uni.FindTranslator(...)
		translator, found := universalTranslator.GetTranslator("en")
		if !found {
			log.Fatal("'en' translator not found")
		}
		if err := translations.RegisterDefaultTranslations(validation, translator); err != nil {
			log.Fatal(err)
		}

		for tag, v := range validators {
			f := v.Func
			msg := v.Message

			if err := validation.RegisterValidation(tag, f); err != nil {
				log.Fatal(fmt.Printf("Could not register validator for '%s'.", tag))
			}

			if msg == "" {
				continue
			}

			registerFn := func(ut ut.Translator) error {
				return ut.Add(tag, msg, true)
			}

			translationFn := func(ut ut.Translator, fe validator.FieldError) string {
				return msg
			}

			if err := validation.RegisterTranslation(tag, translator, registerFn, translationFn); err != nil {
				log.Fatal(err)
			}
		}

		return translator
	} else {
		log.Fatal("Could not configure Gin validator")
	}

	return nil
}

func httpContextProvider() gin.HandlerFunc {
	return func(c *gin.Context) {
		url := c.Request.URL.String()
		c.Set("HttpContext", gin.H{
			"URL": url,
		})
		c.Next()
	}
}

func configureGin(secret []byte, log io.Writer, validators map[string]Validator) (*gin.Engine, ut.Translator) {
	basedir, _ := GetRootDirectory()
	fmt.Println("Basedir:", basedir)

	gob.Register(uuid.UUID{})

	r := gin.New()
	r.Use(gin.LoggerWithWriter(log))
	r.Use(httpContextProvider())

	store := cookie.NewStore(secret)
	r.Use(sessions.Sessions("session", store))
	r.Use(method.ProcessMethodOverride(r))

	r.MaxMultipartMemory = 128 << 20 // 128 MiB
	r.HTMLRender = createRender(basedir)

	configureStaticFiles(basedir, r)
	t := configureValidation(validators)

	return r, t
}
