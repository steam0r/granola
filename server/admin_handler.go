package server

import (
	"archive/zip"
	"bytes"
	"crypto/rand"
	"fmt"
	"io"
	"log"
	"math/big"
	"net/http"
	"sort"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/gosimple/slug"
	"gorm.io/gorm"

	"granola/models"
)

type AdminHandler struct {
	BaseHandler
}

func (h *AdminHandler) showAdmin(c *gin.Context) {
	var userCount int64
	var entryCount int64
	h.DB.Model(&models.User{}).Count(&userCount)
	h.DB.Model(&models.Entry{}).Count(&entryCount)
	voteTotals, err := models.GetVoteTotals(h.DB)

	if err != nil {
		panic(err)
	}

	stats := gin.H{
		"users":   userCount,
		"entries": entryCount,
		"votes":   voteTotals.Votes,
		"score":   voteTotals.Score,
	}
	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Admin",
		"Stats":       &stats,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/admin",
		HTMLData: &data,
		Data:     &stats,
	})
}

func (h *AdminHandler) getInvites(c *gin.Context) {
	var invites []models.Invite
	result := h.DB.Preload("User").Find(&invites)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Invites",
		"Invites":     invites,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/invites",
		HTMLData: &data,
		Data:     &invites,
	})
}

func (h *AdminHandler) postInvite(c *gin.Context) {
	args := struct {
		Key string `json:"key" form:"key" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	invite, err := models.NewInvite(h.DB, args.Key)
	if err != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, err.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/invites")
	} else {
		c.JSON(http.StatusCreated, invite)
	}
}

func (h *AdminHandler) generateInvites(c *gin.Context) {
	args := struct {
		Amount  int      `json:"amount" form:"amount" binding:"required"`
		Prefix  string   `json:"prefix" form:"prefix"`
		Digits  int      `json:"digits" form:"digits" binding:"required"`
		Classes []string `json:"classes" form:"classes[]" binding:"required"`
		Suffix  string   `json:"suffix" form:"suffix"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	randomString := func(digits int, charset []rune) (string, error) {
		var ret []rune
		bigInt := big.NewInt(int64(len(charset) - 1))
		for i := 0; i <= digits; i++ {
			idx, err := rand.Int(rand.Reader, bigInt)
			if err != nil {
				return "", err
			}
			ret = append(ret, charset[idx.Int64()])
		}
		return string(ret), nil
	}

	var charset []rune

	for _, class := range args.Classes {
		if class == "lowercase" {
			for ch := 'a'; ch <= 'z'; ch++ {
				charset = append(charset, ch)
			}
		}
		if class == "uppercase" {
			for ch := 'A'; ch <= 'Z'; ch++ {
				charset = append(charset, ch)
			}
		}
		if class == "digits" {
			for ch := '0'; ch <= '9'; ch++ {
				charset = append(charset, ch)
			}
		}
	}

	for i := 0; i <= args.Amount; i++ {
		key, err := randomString(args.Digits, charset)
		if err != nil {
			h.abortWithStatusAndText(c, http.StatusInternalServerError, err.Error())
			return
		}

		_, err = models.NewInvite(h.DB, args.Prefix+key+args.Suffix)
		if err != nil {
			h.abortWithStatusAndText(c, http.StatusInternalServerError, err.Error())
			return
		}
	}

	c.Redirect(http.StatusFound, "/invites")
}

func (h *AdminHandler) getUsers(c *gin.Context) {
	var users []models.User
	result := h.DB.Preload("Entries").Find(&users)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Users",
		"Users":       users,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/users",
		HTMLData: &data,
		Data:     users,
	})
}

func (h *AdminHandler) postUser(c *gin.Context) {
	args := struct {
		Email  string `json:"email" form:"email" binding:"required"`
		Handle string `json:"handle" form:"handle" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	user, err := models.NewUser(h.DB, args.Email, args.Handle, nil)
	if err != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, err.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/users")
	} else {
		c.JSON(http.StatusCreated, &user)
	}
}

func (h *AdminHandler) getUser(c *gin.Context) {
	user := models.User{}
	if err := h.DB.
		Omit("Content").
		Preload("Entries.Uploads").
		First(&user, "id = ?", c.Param("user_id")).
		Error; err != nil {
		panic(err)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "User",
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/user",
		HTMLData: &data,
		Data:     &user,
	})
}

func (h *AdminHandler) putUser(c *gin.Context) {
	args := struct {
		Email  string `json:"email" form:"email" binding:"required"`
		Handle string `json:"handle" form:"handle" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	user := models.User{}
	result := h.DB.First(&user, "id = ?", c.Param("user_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	user.Email = args.Email
	user.Handle = args.Handle

	result = h.DB.Save(&user)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, user.Path())
	} else {
		c.JSON(http.StatusOK, &user)
	}
}

func (h *AdminHandler) deleteUser(c *gin.Context) {
	result := h.DB.Delete(&models.User{}, "id = ?", c.Param("user_id"))
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	c.Status(http.StatusNoContent)
}

func (h *AdminHandler) getCompos(c *gin.Context) {
	var compos []models.Compo
	result := h.DB.Preload("Entries").Find(&compos)

	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Compos",
		"Compos":      compos,
		"CompoTypes":  models.CompoTypes,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/compos",
		HTMLData: &data,
		Data:     compos,
	})
}

func (h *AdminHandler) postCompo(c *gin.Context) {
	args := struct {
		Name          string           `json:"name" form:"name" binding:"required"`
		MultiPlatform *bool            `json:"multiPlatform" form:"multiplatform" binding:"required"`
		Locked        *bool            `json:"locked" form:"locked"`
		Type          models.CompoType `json:"type" form:"type"`
		Description   string           `json:"description" form:"description"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	locked := false
	if args.Locked != nil {
		locked = *args.Locked
	}

	compo, err := models.NewCompo(
		h.DB,
		args.Name,
		*args.MultiPlatform,
		locked,
		args.Type,
		args.Description)
	if err != nil {
		// TODO: When this fails with `UNIQUE constraint failed: compos.name`
		//       (sqlite3.Error, Code 19, ExtendedCode 2067) we should preferably
		//       fail with a 409 Conflict response explaining that the compo name
		//       is already taken.
		h.abortWithStatusAndText(c, http.StatusInternalServerError, err.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/compos")
	} else {
		c.JSON(http.StatusCreated, compo)
	}
}

func (h *AdminHandler) getCompo(c *gin.Context) {
	compo := models.Compo{}
	result := h.DB.Preload("Entries.Uploads").First(&compo, c.Param("compo_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Compo",
		"Compo":       &compo,
		"CompoTypes":  models.CompoTypes,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/compo",
		HTMLData: &data,
		Data:     &compo,
	})
}

func (h *AdminHandler) putCompo(c *gin.Context) {
	args := struct {
		Name          string           `json:"name" form:"name" binding:"required"`
		MultiPlatform *bool            `json:"multiPlatform" form:"multiplatform" binding:"required"`
		Locked        *bool            `json:"locked" form:"locked" binding:"required"`
		Type          models.CompoType `json:"type" form:"type"`
		Description   string           `json:"description" form:"description"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	compo := models.Compo{}
	result := h.DB.First(&compo, c.Param("compo_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	compo.Name = args.Name
	compo.MultiPlatform = *args.MultiPlatform
	compo.Locked = *args.Locked
	compo.Type = args.Type
	compo.Description = args.Description

	result = h.DB.Save(&compo)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, compo.Path())
	} else {
		c.JSON(http.StatusOK, &compo)
	}
}

func (h *AdminHandler) deleteCompo(c *gin.Context) {
	result := h.DB.Delete(&models.Compo{}, c.Param("compo_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "/compos")
	} else {
		c.Status(http.StatusNoContent)
	}
}

func (h *AdminHandler) getEntries(c *gin.Context) {
	var entries []models.Entry
	result := h.DB.
		Preload("Uploads", func(tx *gorm.DB) *gorm.DB { return tx.Omit("Content") }).
		Preload("Compo").
		Find(&entries)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Entries",
		"Entries":     entries,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/entries",
		HTMLData: &data,
		Data:     entries,
	})
}

func (h *AdminHandler) postCompoEntry(c *gin.Context) {
	args := struct {
		Title    string `json:"title" form:"title" binding:"required"`
		Author   string `json:"author" form:"author" binding:"required"`
		Platform string `json:"platform" form:"platform"`
		Notes    string `json:"notes" form:"notes"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	compo := models.Compo{}
	result := h.DB.First(&compo, c.Param("compo_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	entry, err := models.NewEntry(h.DB, args.Title, args.Author, args.Platform, args.Notes, &compo, &models.User{}, false)
	if err != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, compo.Path())
	} else {
		c.JSON(http.StatusCreated, &entry)
	}
}

func (h *AdminHandler) getEntry(c *gin.Context) {
	entry := models.Entry{}
	result := h.DB.
		Preload("Uploads", func(tx *gorm.DB) *gorm.DB { return tx.Omit("Content") }).
		Preload("Uploads.Entry").
		Preload("Compo").
		First(&entry, "id = ?", c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	var compos []models.Compo
	result = h.DB.Find(&compos)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Entry",
		"Entry":       &entry,
		"Compos":      compos,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/entry",
		HTMLData: &data,
		Data:     &entry,
	})
}

func (h *AdminHandler) getEntryPreview(c *gin.Context) {
	entry := models.Entry{}
	result := h.DB.
		Preload("Compo").
		Preload("User").
		Preload("Uploads.Entry").
		First(&entry, "id = ?", c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Preview Entry",
		"Entry":       &entry,
		"EntryNumber": 0,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.HTML(http.StatusOK, "entry-preview", &data)
}

func (h *AdminHandler) getPlaylistEntry(c *gin.Context) {
	playlistEntry := models.PlaylistEntry{}
	result := h.DB.
		Preload("Playlist").
		Preload("Entry.Uploads").
		Where("playlist_id = ? and entry_id = ?",
			c.Param("playlist_id"),
			c.Param("entry_id")).
		First(&playlistEntry)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Preview Entry",
		"Entry":       &playlistEntry.Entry,
		"EntryNumber": playlistEntry.Order,
		"Playlist":    &playlistEntry.Playlist,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.HTML(http.StatusOK, "entry-preview", &data)
}

func (h *AdminHandler) putEntry(c *gin.Context) {
	args := struct {
		Title    string `json:"title" form:"title" binding:"required"`
		Author   string `json:"author" form:"author" binding:"required"`
		Platform string `json:"platform" form:"platform"`
		Notes    string `json:"notes" form:"notes"`
		CompoID  uint   `json:"compo" form:"compo" binding:"required"`
		Locked   *bool  `json:"locked" form:"locked" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	compo := models.Compo{}
	result := h.DB.First(&compo, args.CompoID)
	if result.Error != nil {
		panic(fmt.Errorf("compo %d not found", args.CompoID))
	}

	entry := models.Entry{}
	result = h.DB.
		Preload("Uploads", func(tx *gorm.DB) *gorm.DB { return tx.Omit("Content") }).
		First(&entry, "id = ?", c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	entry.Title = args.Title
	entry.Author = args.Author
	entry.Platform = args.Platform
	entry.Notes = args.Notes
	entry.Compo = compo
	entry.Locked = *args.Locked

	result = h.DB.Save(&entry)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, entry.Path())
	} else {
		c.JSON(http.StatusOK, &entry)
	}
}

func (h *AdminHandler) deleteEntry(c *gin.Context) {
	result := h.DB.Delete(&models.Entry{}, "id = ?", c.Param("entry_id"))

	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.Status(http.StatusNoContent)
	}
}

func (h *AdminHandler) getUploads(c *gin.Context) {
	var uploads []models.Upload
	if err := h.DB.
		Omit("Content").
		Preload("Entry").
		Where("entry_id = ?", c.Param("entry_id")).
		Find(&uploads).
		Error; err != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, err.Error())
		return
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Uploads",
		"Uploads":     uploads,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/uploads",
		HTMLData: &data,
		Data:     uploads,
	})
}

func (h *AdminHandler) postUpload(c *gin.Context) {
	formFile, err := c.FormFile("file")
	if err != nil {
		panic(err)
	}
	file, err := formFile.Open()
	if err != nil {
		panic(err)
	}

	content, err := io.ReadAll(file)
	if err != nil {
		panic(err)
	}

	entry := &models.Entry{}
	result := h.DB.
		Preload("Uploads", func(tx *gorm.DB) *gorm.DB { return tx.Omit("Content") }).
		First(&entry, "id = ?", c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	upload, err := models.NewUpload(h.DB, formFile.Filename, content, entry)
	if err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, upload.Path())
	} else {
		c.JSON(http.StatusCreated, &upload)
	}
}

func (h *AdminHandler) getUpload(c *gin.Context) {
	upload := models.Upload{}
	result := h.DB.Omit("Content").
		Preload("Entry").
		Preload("Entry.Compo").
		Where("entry_id = ?", c.Param("entry_id")).
		First(&upload, "id = ?", c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Upload",
		"Upload":      &upload,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/upload",
		HTMLData: &data,
		Data:     &upload,
	})
}

func (h *AdminHandler) getUploadData(c *gin.Context) {
	upload := models.Upload{}
	result := h.DB.Where("entry_id = ?", c.Param("entry_id")).
		First(&upload, "id = ?", c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}
	c.Header("Content-Disposition", "attachment; filename="+upload.Filename)
	c.Data(http.StatusOK, "application/octet-stream", upload.Content)
}

func (h *AdminHandler) deleteUpload(c *gin.Context) {
	result := h.DB.
		Where("entry_id = ?", c.Param("entry_id")).
		Delete(&models.Upload{}, "id = ?", c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.Status(http.StatusNoContent)
	}
}

func (h *AdminHandler) getPlaylists(c *gin.Context) {
	var playlists []models.Playlist
	result := h.DB.Preload("Entries").Find(&playlists)

	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Playlists",
		"Playlists":   playlists,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/playlists",
		HTMLData: &data,
		Data:     playlists,
	})
}

func (h *AdminHandler) putPlaylist(c *gin.Context) {
	args := struct {
		Name string `json:"name" form:"name" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	playlist := models.Playlist{}
	result := h.DB.First(&playlist, c.Param("playlist_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	playlist.Name = args.Name
	result = h.DB.Save(&playlist)
	if result.Error != nil {
		panic(result.Error)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, playlist.Path()+"?"+c.Request.URL.RawQuery)
	} else {
		c.JSON(http.StatusOK, &playlist)
	}
}

func (h *AdminHandler) postPlaylist(c *gin.Context) {
	args := struct {
		Name    string `json:"name" form:"name" binding:"required"`
		Entries []struct {
			EntryID uuid.UUID `json:"entry" binding:"required"`
		} `json:"entries"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	var entries []uuid.UUID
	for _, e := range args.Entries {
		entries = append(entries, e.EntryID)
	}

	playlist, err := models.NewPlaylist(h.DB, args.Name, entries)
	if err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, playlist.Path()+"?"+c.Request.URL.RawQuery)
	} else {
		c.JSON(http.StatusCreated, &playlist)
	}
}

func (h *AdminHandler) patchPlaylists(c *gin.Context) {
	type PlaylistAction string
	const (
		Close PlaylistAction = "close"
	)
	args := struct {
		Action PlaylistAction `form:"action" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	var playlists []models.Playlist
	now := time.Now()
	tx := h.DB.Begin()
	if err := tx.
		Model(&playlists).
		Where("voting_closed_at is null").
		UpdateColumn("voting_closed_at", now).
		Error; err != nil {
		tx.Rollback()
		panic(err)
	}

	if err := tx.
		Model(&playlists).
		Where("live_voting_closed_at is null").
		UpdateColumn("live_voting_closed_at", now).
		Error; err != nil {
		tx.Rollback()
		panic(err)
	}

	if err := tx.Find(&playlists).Error; err != nil {
		tx.Rollback()
		panic(err)
	}

	if err := tx.Commit().Error; err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/playlists")
	} else {
		c.JSON(http.StatusOK, &playlists)
	}
}

func (h *AdminHandler) postPlaylistEntry(c *gin.Context) {
	args := struct {
		// Can't bind to uuid.UUID because of this:
		// https://github.com/gin-gonic/gin/issues/2673
		EntryID string `json:"entry" form:"entry" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	entryUUID, err := uuid.Parse(args.EntryID)
	if err != nil {
		panic(err)
	}

	playlist := &models.Playlist{}
	playlistId := c.Param("playlist_id")
	tx := h.DB.Begin()
	if err := tx.First(&playlist, playlistId).Error; err != nil {
		tx.Rollback()
		panic(err)
	}

	entry := &models.Entry{}
	// A foreign constraint should check this, but I can't seem to get that to work with SQLite.
	if err := tx.First(&entry, "id = ?", entryUUID).Error; err != nil {
		tx.Rollback()
		panic(err)
	}

	playlistEntryCount := uint(0)
	if err := tx.
		Table("playlist_entries").
		Where("playlist_id = ?", playlistId).
		Select("count(entry_id)").
		Row().
		Scan(&playlistEntryCount); err != nil {
		playlistEntryCount = 0
	}

	playlistEntry, err := models.NewPlaylistEntry(tx, playlist, entry, playlistEntryCount)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	if err := tx.Commit().Error; err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, playlist.Path()+"?"+c.Request.URL.RawQuery)
	} else {
		c.JSON(http.StatusCreated, &playlistEntry)
	}
}

func (h *AdminHandler) patchPlaylistEntry(c *gin.Context) {
	type Direction string
	const (
		Up   Direction = "up"
		Down Direction = "down"
	)
	args := struct {
		Direction Direction `json:"direction" form:"direction" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	uriArgs := struct {
		// Can't bind to uuid.UUID because of this:
		// https://github.com/gin-gonic/gin/issues/2673
		EntryID string `uri:"entry_id" binding:"required,uuid"`
	}{}
	if err := c.ShouldBindUri(&uriArgs); err != nil {
		panic(err)
	}

	var playlistEntries []models.PlaylistEntry
	result := h.DB.
		Preload("Playlist").
		Preload("Entry").
		Order("`order` asc").
		Find(&playlistEntries, "playlist_id = ?", c.Param("playlist_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	index, playlistEntry := func() (int, models.PlaylistEntry) {
		u := uuid.MustParse(uriArgs.EntryID)
		for i, entry := range playlistEntries {
			if entry.EntryID == u {
				return i, entry
			}
		}
		panic("entry not found")
	}()

	if reorderedPlaylistEntry := func() *models.PlaylistEntry {
		switch args.Direction {
		case Up:
			if index > 0 {
				return &playlistEntries[index-1]
			}
		case Down:
			if index < len(playlistEntries)-1 {
				return &playlistEntries[index+1]
			}
		}

		return nil
	}(); reorderedPlaylistEntry != nil {
		reorderedPlaylistEntry.Order, playlistEntry.Order = playlistEntry.Order, reorderedPlaylistEntry.Order
		tx := h.DB.Begin()
		tx.Save(&playlistEntry)
		tx.Save(&reorderedPlaylistEntry)
		result := tx.Commit()
		if result.Error != nil {
			panic(result.Error)
		}
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, playlistEntry.Playlist.Path()+"?"+c.Request.URL.RawQuery)
	} else {
		c.JSON(http.StatusOK, &playlistEntry)
	}
}

func (h *AdminHandler) getPlaylistEntries(c *gin.Context) {
	var playlistEntries []models.PlaylistEntry
	if err := h.DB.
		Preload("Playlist").
		Preload("Entry").
		Order("`order` asc").
		Find(&playlistEntries, "playlist_id = ?", c.Param("playlist_id")).
		Error; err != nil {
		panic(err)
	}
	c.JSON(http.StatusOK, &playlistEntries)
}

func (h *AdminHandler) deletePlaylistEntry(c *gin.Context) {
	result := h.DB.
		Where("playlist_id = ? and entry_id = ?",
			c.Param("playlist_id"),
			c.Param("entry_id")).
		Delete(&models.PlaylistEntry{})
	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.Status(http.StatusNoContent)
	}
}

func (h *AdminHandler) getPlaylist(c *gin.Context) {
	playlist := models.Playlist{}
	result := h.DB.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry").
		Preload("Entries.Playlist").
		First(&playlist, c.Param("playlist_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	var compos []models.Compo
	if result := h.DB.Find(&compos); result.Error != nil {
		panic(result.Error)
	}

	var selectedCompo *models.Compo
	h.DB.Preload("Entries").First(&selectedCompo, c.Query("compo"))

	results, err := h.getPlaylistResult(&playlist)
	if err != nil {
		panic(err)
	}

	data := gin.H{
		"Config":        &h.Config.Routing,
		"Title":         "Playlist",
		"Playlist":      &playlist,
		"Results":       results,
		"Compos":        compos,
		"SelectedCompo": selectedCompo,
		"HttpContext":   c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/playlist",
		HTMLData: &data,
		Data:     &playlist,
	})
}

func (h *AdminHandler) getPlaylistResults(c *gin.Context) {
	playlist := models.Playlist{}
	result := h.DB.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry").
		Preload("Entries.Playlist").
		First(&playlist, c.Param("playlist_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	results, err := h.getPlaylistResult(&playlist)
	if err != nil {
		panic(err)
	}
	c.JSON(http.StatusOK, results)
}

func (h *AdminHandler) getPlaylistArchive(c *gin.Context) {
	// TODO: Combine this with getCompoArchive() somehow to avoid (this much)
	//       code duplication.
	query := struct {
		Ordered bool `form:"ordered"`
	}{}
	if err := c.ShouldBindQuery(&query); err != nil {
		panic(err)
	}

	playlist := models.Playlist{}
	if err := h.DB.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry.Uploads").
		Preload("Entries.Playlist").
		First(&playlist, c.Param("playlist_id")).
		Error; err != nil {
		panic(err)
	}

	c.Writer.Header().Set("Content-Type", "application/zip")
	c.Writer.Header().Set("Content-Disposition", "attachment; filename="+slug.Make(playlist.Name)+".zip")
	// TODO: Test this. For some reason, invoking c.Stream() causes the following
	//       error in the test suite (but not in the browser):
	//       interface conversion: *httptest.ResponseRecorder is not http.CloseNotifier:
	//       missing method CloseNotify
	c.Stream(func(w io.Writer) bool {
		playlistArchive := zip.NewWriter(w)
		defer func(playlistArchive *zip.Writer) {
			if err := playlistArchive.Close(); err != nil {
				log.Print(err)
			}
		}(playlistArchive)

		for _, entry := range playlist.Entries {
			if len(entry.Entry.Uploads) == 0 {
				// If there are no uploads, skip this entry.
				continue
			}

			if !query.Ordered && len(entry.Entry.Uploads) == 1 {
				// If we don't want an ordered archive, and if there's only one
				// upload, don't create a zip file for it.
				upload := entry.Entry.Uploads[0]
				if f, err := playlistArchive.Create(upload.Filename); err != nil {
					log.Print(err)
					return false
				} else if _, err := f.Write(upload.Content); err != nil {
					log.Print(err)
					return false
				}
				continue
			}

			func() {
				// Otherwise, create a zip file that contains all uploads for the entry.
				entryBuffer := &bytes.Buffer{}
				entryArchive := zip.NewWriter(entryBuffer)
				defer func(entryArchive *zip.Writer) {
					if err := entryArchive.Close(); err != nil {
						log.Print(err)
					}
				}(entryArchive)

				entryFilename := fmt.Sprintf("%s-%s", slug.Make(entry.Entry.Author), slug.Make(entry.Entry.Title))

				if query.Ordered {
					entryFilename = fmt.Sprintf("%02d_%s", entry.Order, entryFilename)
				}

				for _, upload := range entry.Entry.Uploads {
					filename := fmt.Sprintf("%s/%s", entryFilename, upload.Filename)
					if f, err := entryArchive.Create(filename); err != nil {
						log.Print(err)
						return
					} else if _, err := f.Write(upload.Content); err != nil {
						log.Print(err)
						return
					}
				}

				if f, err := playlistArchive.Create(entryFilename + ".zip"); err != nil {
					log.Print(err)
				} else if _, err := f.Write(entryBuffer.Bytes()); err != nil {
					log.Print(err)
				}
			}()
		}

		return false
	})
}

func (h *AdminHandler) getCompoArchive(c *gin.Context) {
	// TODO: Combine this with getPlaylistArchive() somehow to avoid (this much)
	//       code duplication.
	compo := models.Compo{}
	if err := h.DB.
		Preload("Entries").
		Preload("Entries.Uploads").
		First(&compo, c.Param("compo_id")).
		Error; err != nil {
		panic(err)
	}

	c.Writer.Header().Set("Content-Type", "application/zip")
	c.Writer.Header().Set("Content-Disposition", "attachment; filename="+slug.Make(compo.Name)+".zip")
	// TODO: Test this. For some reason, invoking c.Stream() causes the following
	//       error in the test suite (but not in the browser):
	//       interface conversion: *httptest.ResponseRecorder is not http.CloseNotifier:
	//       missing method CloseNotify
	c.Stream(func(w io.Writer) bool {
		compoArchive := zip.NewWriter(w)
		defer func(compoArchive *zip.Writer) {
			if err := compoArchive.Close(); err != nil {
				log.Print(err)
			}
		}(compoArchive)

		for _, entry := range compo.Entries {
			if len(entry.Uploads) == 0 {
				// If there are no uploads, skip this entry.
				continue
			}

			if len(entry.Uploads) == 1 {
				// If there's only one upload, don't create a zip file for it.
				upload := entry.Uploads[0]
				if f, err := compoArchive.Create(upload.Filename); err != nil {
					fmt.Print(err)
					return false
				} else if _, err := f.Write(upload.Content); err != nil {
					fmt.Print(err)
					return false
				}
				continue
			}

			func() {
				// Otherwise, create a zip file that contains all uploads for the entry.
				entryBuffer := &bytes.Buffer{}
				entryArchive := zip.NewWriter(entryBuffer)
				defer func(entryArchive *zip.Writer) {
					if err := entryArchive.Close(); err != nil {
						fmt.Print(err)
					}
				}(entryArchive)

				entryFilename := fmt.Sprintf("%s-%s", slug.Make(entry.Author), slug.Make(entry.Title))
				for _, upload := range entry.Uploads {
					filename := fmt.Sprintf("%s/%s", entryFilename, upload.Filename)
					if f, err := entryArchive.Create(filename); err != nil {
						fmt.Print(err)
						return
					} else if _, err := f.Write(upload.Content); err != nil {
						fmt.Print(err)
						return
					}
				}

				if f, err := compoArchive.Create(entryFilename + ".zip"); err != nil {
					fmt.Print(err)
				} else if _, err := f.Write(entryBuffer.Bytes()); err != nil {
					fmt.Print(err)
				}
			}()
		}

		return false
	})
}

func (h *AdminHandler) deletePlaylist(c *gin.Context) {
	result := h.DB.Delete(&models.Playlist{}, c.Param("playlist_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.Status(http.StatusNoContent)
	}
}

func (h *AdminHandler) getPlaylistPreview(c *gin.Context) {
	playlist, slideNumber := h.getPlaylistAndSlideNumber(c)
	playlistPreview := models.PlaylistPreview{
		Playlist:    playlist,
		SlideNumber: slideNumber,
	}
	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Preview Playlist",
		"Preview":     &playlistPreview,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/playlist-preview",
		HTMLData: &data,
		Data:     &playlistPreview,
	})
}

func (h *AdminHandler) patchPlaylist(c *gin.Context) {
	type PlaylistAction string
	const (
		Show  PlaylistAction = "show"
		Close PlaylistAction = "close"
		Open  PlaylistAction = "open"
	)
	args := struct {
		Action PlaylistAction `form:"action" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	playlist := models.Playlist{}
	tx := h.DB.Begin()
	if err := tx.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry.Compo").
		First(&playlist, c.Param("playlist_id")).
		Error; err != nil {
		tx.Rollback()
		panic(err)
	}

	playlistShow, redirectUrl, err := func() (*models.PlaylistShow, string, error) {
		action := playlist.Show
		getPath := func(models.Playlist, *models.PlaylistShow) string {
			return playlist.Path()
		}

		switch args.Action {
		case Show:
			getPath = func(playlist models.Playlist, playlistShow *models.PlaylistShow) string {
				return playlistShow.Path()
			}
		case Close:
			action = playlist.Close
		case Open:
			action = playlist.Open
		default:
			action = func(db *gorm.DB) (*models.PlaylistShow, error) {
				return nil, fmt.Errorf("invalid action: %v", args.Action)
			}
		}

		if playlistShow, err := action(tx); err != nil {
			return nil, "", err
		} else {
			return playlistShow, getPath(playlist, playlistShow), err
		}
	}()

	if err != nil {
		tx.Rollback()
		panic(err)
	}

	if err := tx.Commit().Error; err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, redirectUrl)
	} else {
		c.JSON(http.StatusOK, playlistShow)
	}
}

func (h *AdminHandler) getPlaylistShow(c *gin.Context) {
	playlist, slideNumber := h.getPlaylistAndSlideNumber(c)
	playlistShow := models.PlaylistShow{
		Playlist:    playlist,
		SlideNumber: slideNumber,
	}
	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Show Playlist",
		"Show":        &playlistShow,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/playlist-show",
		HTMLData: &data,
		Data:     &playlistShow,
	})
}

func (h *AdminHandler) postPlaylistShowSlide(c *gin.Context) {
	type PlaylistAction string
	const (
		Show PlaylistAction = "show"
	)
	args := struct {
		Action PlaylistAction `form:"action" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	if args.Action != Show {
		panic(fmt.Errorf("invalid action: %v", args.Action))
	}

	playlist, slideNumber := h.getPlaylistAndSlideNumber(c)
	playlistEntry := playlist.Entries[slideNumber-1]
	if err := playlistEntry.Show(h.DB); err != nil {
		panic(err)
	}

	playlistShow := models.PlaylistShow{
		Playlist:    playlist,
		SlideNumber: slideNumber,
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, playlistShow.Path())
	} else {
		c.JSON(http.StatusOK, &playlistShow)
	}
}

func (h *AdminHandler) getPlaylistAndSlideNumber(c *gin.Context) (models.Playlist, int) {
	playlist := models.Playlist{}
	result := h.DB.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry.Uploads.Entry").
		Preload("Entries.Playlist").
		First(&playlist, c.Param("playlist_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	uriArgs := struct {
		SlideNumber int `uri:"slide_number"`
	}{}
	if err := c.ShouldBindUri(&uriArgs); err != nil {
		panic(err)
	}

	slideNumber := max(min(uriArgs.SlideNumber, len(playlist.Entries)), 1)

	return playlist, slideNumber
}

type EntryWithScore struct {
	Entry models.Entry
	Score uint
}

type PlaylistResult struct {
	Playlist models.Playlist
	Results  [][]EntryWithScore
}

func (h *AdminHandler) getPlaylistResult(playlist *models.Playlist) (PlaylistResult, error) {
	var users []models.User
	result := h.DB.Find(&users)
	if result.Error != nil {
		return PlaylistResult{}, result.Error
	}

	entryMap := map[uint][]EntryWithScore{}
	for _, e := range playlist.Entries {
		var sum uint
		for _, u := range users {
			vote, err := u.CurrentVote(h.DB, e.EntryID)
			if err != nil {
				continue
			}
			sum = sum + vote.Score
		}
		entryWithScore := EntryWithScore{
			Entry: e.Entry,
			Score: sum,
		}
		if val, ok := entryMap[sum]; ok {
			entryMap[sum] = append(val, entryWithScore)
		} else {
			entryMap[sum] = []EntryWithScore{entryWithScore}
		}
	}

	var results [][]EntryWithScore
	for _, result := range entryMap {
		results = append([][]EntryWithScore{result}, results...)
	}

	sort.SliceStable(results, func(i, j int) bool {
		return results[i][0].Score >= results[j][0].Score
	})

	return PlaylistResult{
		Playlist: *playlist,
		Results:  results,
	}, nil
}

func (h *AdminHandler) getResults(c *gin.Context) {
	var playlists []models.Playlist
	result := h.DB.Preload("Entries").
		Preload("Entries.Entry").
		Find(&playlists)
	if result.Error != nil {
		panic(result.Error)
	}

	var results []PlaylistResult
	for _, playlist := range playlists {
		result, err := h.getPlaylistResult(&playlist)
		if err != nil {
			panic(err)
		}
		results = append(results, result)
	}

	c.Writer.Header().Set("Content-Type", "text/plain")
	if _, ok := c.GetQuery("download"); ok {
		c.Header("Content-Disposition", "attachment; filename=results.txt")
	}
	c.HTML(http.StatusOK, "admin/_results", results)
}

func (h *AdminHandler) getPrizegiving(c *gin.Context) {
	playlists := []models.Playlist{}
	result := h.DB.Preload("Entries").Find(&playlists)

	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Prizegiving",
		"Playlists":   playlists,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/prizegiving",
		HTMLData: &data,
		Data:     playlists,
	})
}

func (h *AdminHandler) getPrizegivingPreview(c *gin.Context) {

	playlists := []models.Playlist{}
	result := h.DB.Preload("Entries").Find(&playlists)

	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Preview Prizegiving",
		"PlayLists":   playlists,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/prizegiving-preview",
		HTMLData: &data,
		Data:     playlists,
	})
}

func (h *AdminHandler) getPrizegivingShow(c *gin.Context) {
	playlist, slideNumber := h.getPlaylistAndSlideNumber(c)
	playlistShow := models.PlaylistShow{
		Playlist:    playlist,
		SlideNumber: slideNumber,
	}
	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Show Playlist",
		"Show":        &playlistShow,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/prizegiving-show",
		HTMLData: &data,
		Data:     &playlistShow,
	})
}

func (h *AdminHandler) getPrizegivingShowSlide(c *gin.Context) {

	playlist := models.Playlist{}
	result := h.DB.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry").
		Preload("Entries.Playlist").
		First(&playlist, c.Param("playlist_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	results, err := h.getPlaylistResult(&playlist)
	if err != nil {
		panic(err)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Preview Entry",
		"Playlist":    &playlist,
		"Results":     results,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.HTML(http.StatusOK, "prizegiving-preview", &data)
}

func (h *AdminHandler) RegisterRoutes(r *gin.Engine, admins map[string]string) {
	admin := r.Group("", gin.BasicAuth(admins))
	admin.GET("", h.showAdmin)

	invites := admin.Group("/invites")
	invites.GET("", h.getInvites)
	invites.POST("", h.postInvite)
	invites.POST("/generate", h.generateInvites)

	users := admin.Group("/users")
	users.GET("", h.getUsers)
	users.POST("", h.postUser)

	user := users.Group("/:user_id")
	user.GET("", h.getUser)
	user.PUT("", h.putUser)
	user.DELETE("", h.deleteUser)

	compos := admin.Group("/compos")
	compos.GET("", h.getCompos)
	compos.POST("", h.postCompo)

	compo := compos.Group("/:compo_id")
	compo.GET("", h.getCompo)
	compo.GET("/archive", h.getCompoArchive)
	compo.PUT("", h.putCompo)
	compo.DELETE("", h.deleteCompo)
	compo.POST("/entries", h.postCompoEntry)

	entries := admin.Group("/entries")
	entries.GET("", h.getEntries)

	entry := entries.Group("/:entry_id")
	entry.GET("", h.getEntry)
	entry.GET("/preview", h.getEntryPreview)
	entry.PUT("", h.putEntry)
	entry.DELETE("", h.deleteEntry)

	uploads := entry.Group("/uploads")
	uploads.GET("", h.getUploads)
	uploads.POST("", h.postUpload)

	upload := uploads.Group("/:upload_id")
	upload.GET("", h.getUpload)
	upload.DELETE("", h.deleteUpload)
	upload.GET("/data", h.getUploadData)

	playlists := admin.Group("/playlists")
	playlists.GET("", h.getPlaylists)
	playlists.PATCH("", h.patchPlaylists)
	playlists.POST("", h.postPlaylist)

	playlist := playlists.Group("/:playlist_id")
	playlist.GET("", h.getPlaylist)
	playlist.GET("/archive", h.getPlaylistArchive)
	playlist.GET("/results", h.getPlaylistResults)
	playlist.PUT("", h.putPlaylist)
	playlist.PATCH("", h.patchPlaylist)
	playlist.DELETE("", h.deletePlaylist)

	playlistEntries := playlist.Group("/entries")
	playlistEntries.GET("", h.getPlaylistEntries)
	playlistEntries.POST("", h.postPlaylistEntry)

	playlistEntry := playlistEntries.Group("/:entry_id")
	playlistEntry.GET("", h.getPlaylistEntry)
	playlistEntry.PATCH("", h.patchPlaylistEntry)
	playlistEntry.DELETE("", h.deletePlaylistEntry)

	playlistPreview := playlist.Group("/preview")
	playlistPreview.GET("", h.getPlaylistPreview)
	playlistPreview.GET("/:slide_number", h.getPlaylistPreview)

	playlistShow := playlist.Group("/show")
	playlistShow.GET("", h.getPlaylistShow)

	playlistShowSlide := playlistShow.Group("/:slide_number")
	playlistShowSlide.GET("", h.getPlaylistShow)
	playlistShowSlide.POST("", h.postPlaylistShowSlide)

	admin.GET("/results", h.getResults)

	prizegiving := admin.Group("/prizegiving")
	prizegiving.GET("", h.getPrizegiving)

	prizegivingPreview := prizegiving.Group("/preview")
	prizegivingPreview.GET("", h.getPrizegivingPreview)

	prizegivingShow := prizegiving.Group("/show")
	prizegivingShow.GET("", h.getPrizegivingShow)

	prizegivingShowSlide := prizegiving.Group("/:playlist_id")
	prizegivingShowSlide.GET("", h.getPrizegivingShowSlide)

}
