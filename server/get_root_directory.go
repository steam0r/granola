package server

import (
	"errors"
	"os"
	"path/filepath"
)

func GetRootDirectory() (string, error) {
	searchDirectory, err := os.Getwd()
	if err != nil {
		return "", err
	}

	for searchDirectory != "" && searchDirectory != "/" {
		goModPath := filepath.Join(searchDirectory, "go.mod")
		stat, err := os.Stat(goModPath)
		if err == nil && stat != nil && !stat.IsDir() {
			return searchDirectory, nil
		}
		searchDirectory = filepath.Dir(searchDirectory)
	}
	return "", errors.New("root directory not found")
}
