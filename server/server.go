package server

import (
	"fmt"
	"net/mail"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"

	"granola/config"
)

type Server struct {
	Engine       *gin.Engine
	UserHandler  UserHandler
	AdminHandler AdminHandler
}

func NewServer(config config.Config, db *gorm.DB) Server {
	r, t := configureGin(config.Secret, config.Logging.Writer, map[string]Validator{
		"email": {
			Func: func(fl validator.FieldLevel) bool {
				field, ok := fl.Field().Interface().(string)
				_, err := mail.ParseAddress(field)

				return ok && err == nil
			},
			Message: "Invalid e-mail address",
		},
		"password": {
			Func: func(fl validator.FieldLevel) bool {
				field, ok := fl.Field().Interface().(string)
				return ok && len(field) >= config.Routing.MinimumPasswordLength
			},
			Message: fmt.Sprintf("Password must be at least %d characters long", config.Routing.MinimumPasswordLength),
		},
	})

	base := BaseHandler{
		Translator: t,
		Config:     config,
		DB:         db,
	}

	s := Server{
		Engine: r,

		UserHandler: UserHandler{
			BaseHandler: base,
		},
		AdminHandler: AdminHandler{
			BaseHandler: base,
		},
	}

	r.Use(gin.CustomRecoveryWithWriter(config.Logging.Writer, base.recovery))
	r.NoRoute(base.show404)

	return s
}

func UserServer(config config.Config, db *gorm.DB) {
	g := NewServer(config, db)
	g.UserHandler.playlistStreamer = NewPlaylistStreamer()
	g.Engine.Use(g.UserHandler.CheckAuth)
	g.UserHandler.RegisterRoutes(g.Engine)

	go g.UserHandler.streamEntries()

	if err := g.Engine.Run(":8080"); err != nil {
		panic(err)
	}
}

func AdminServer(config config.Config, db *gorm.DB) {
	g := NewServer(config, db)
	g.AdminHandler.RegisterRoutes(g.Engine, config.Admins)
	if err := g.Engine.Run(":8008"); err != nil {
		panic(err)
	}
}
