(function() {
  document.addEventListener('DOMContentLoaded', function() {
    document.addEventListener('keydown', (event) => {
      const h = window.location.hash;
      // If:
      //   1. Escape is pressed,
      //   2. A hash is present in the URL,
      //   3. And an element identified by the hash exists;
      // Clear the hash from the URL, effectively removing the :target of the
      // dialog identified by the hash and thus closing the dialog.
      if (event.key === 'Escape' && h && h.length > 1 && document.getElementById(h.substring(1))) {
        window.location.href = window.location.href.replace(h, '#');
      }
    })

    document.querySelectorAll('.deletable form').forEach(function(form) {
      if (!form.querySelector('input[name="_method"][value="DELETE"]')) {
        return;
      }

      form.addEventListener('submit', function(event) {
        event.preventDefault();

        fetch(form.action, { method: 'DELETE' }).then((response) => {
          if (response.ok) {
            let p = form;

            while (p = p.parentNode) {
              if (p.classList && !p.classList.contains('deletable')) {
                continue;
              }

              p.classList.add('deleting');

              if (window.location.hash && window.location.hash.length > 1) {
                window.location.href = window.location.href.replace(window.location.hash, '#');
              }

              return setTimeout(() =>
              {
                p.remove();
                if (!document.querySelectorAll('.deletable').length) {
                  document.querySelector('#content').classList.add('empty');
                }
                return Promise.resolve();
              }, 1000);
            }
          } else {
            return Promise.reject(response);
          }
        });
      });
    });

    const password = document.getElementById('password');
    const passwordConfirm = document.getElementById('password-confirm');
    const passwordConfirmWrapper = document.getElementById('password-confirm-wrapper');

    if (password && passwordConfirm && passwordConfirmWrapper) {
      const validatePassword = () => {
        const customValidity = password.value == passwordConfirm.value
          ? ''
          : 'Passwords don\'t match';

        passwordConfirm.setCustomValidity(customValidity);
      };

      password.addEventListener('change', validatePassword);
      passwordConfirm.addEventListener('keyup', validatePassword);
      passwordConfirmWrapper.style.display = 'block';
    }

    document.querySelectorAll('.clipboard-copy').forEach(function(element) {
      element.addEventListener('click', function(event) {
        navigator.clipboard.writeText(element.innerText);
      });
    });

    const compo = document.getElementById('compo')
    const platform = document.getElementById('platform');
    if (compo && platform) {
      function updateCompo() {
        const multiPlatform = compo.options[compo.selectedIndex].dataset.multiplatform;
        platform.disabled = !multiPlatform;
      }

      if (compo.options[compo.selectedIndex].disabled)
        platform.disabled = true; // nothing selected
      else
        updateCompo();

      compo.addEventListener('change', updateCompo);
    }

    document.querySelectorAll('.entry-frame > iframe').forEach(function(iframe) {
      iframe.style.visibility = 'hidden';
      iframe.onload = function(event) {
        this.style.visibility = 'visible';
      };
    })
  });
})();
